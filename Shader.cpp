#include "Shader.h"

#include <fstream>

#include <Eigen/Core>


namespace  { // anonymous namespace

// ------------------- Compile Shader ------------------------------------------------//

void readShaderWithIncludes(const std::string &shaderPath, std::string &sourceCode) {
    const std::string shaderFullPath = "../sophie/shaders/" + shaderPath;

    if (sourceCode.empty()) {
        sourceCode = "#version 460 core\n";
    }

    // Open file
    std::ifstream file{shaderFullPath, std::fstream::in};
    if (!file) {
        throw Shader::ErrorShader{"The file " + shaderFullPath + " could not have been opened"};
    }

    // Read includes
    bool isComment = true;
    std::string line;
    std::string firstPart, secondPart;
    std::streampos lastCommentLinePosition = file.tellg();

    while (isComment && getline(file, line)) {
        std::stringstream streamLine{line};
        if (streamLine >> firstPart >> secondPart) {
            const std::string firstTwoCharacters = firstPart.substr(0, 2);
            isComment = (firstTwoCharacters == "//");
            if (isComment) {
                lastCommentLinePosition = file.tellg();
                if (firstPart == "//#include") {
                    readShaderWithIncludes(secondPart, sourceCode);
                }
            }
        }
    }

    file.seekg(lastCommentLinePosition); // Set file position to last comment line

    // Finish to read the file
    constexpr unsigned int bufferSize = 2000;
    unsigned long sourceCodeSize = sourceCode.size();

    while (file) {
        sourceCode.resize(sourceCodeSize + bufferSize);
        file.read(&sourceCode[sourceCodeSize], bufferSize);
        sourceCodeSize += static_cast<unsigned long>(file.gcount());
    }
    sourceCode.resize(sourceCodeSize);

    file.close();
}


// TODO : Afficher les warning des shaders qui ne sont pas detectés avec les success, chercher le mot warning dans le message
void check_compilationShader (const GLuint &shader, const std::string &name_shader) {
    int success;
    glGetShaderiv(shader, GL_COMPILE_STATUS, &success);

    if (!success) {
        GLint size_infoLog;
        glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &size_infoLog);

        auto *infoLog = new GLchar[size_infoLog];
        glGetShaderInfoLog(shader, size_infoLog, nullptr, infoLog);
        const std::string infoLogString{infoLog};
        delete[] infoLog;

        throw Shader::ErrorShader{"The shader's compilation " + name_shader + " failed :\n" + infoLogString};
    }
}


GLuint createShader(const std::string &pathShader, GLenum shaderType) {
    const GLuint shaderID = glCreateShader(shaderType);
    std::string sourceCode;
    readShaderWithIncludes(pathShader, sourceCode);
    const char *sourceCodeCast = sourceCode.c_str();
    glShaderSource(shaderID, 1, &sourceCodeCast, nullptr);

    glCompileShader(shaderID);
    check_compilationShader(shaderID, pathShader);

    return shaderID;
}


// TODO : taille de message dynamique
void check_linkShaders (GLuint shaderProgram) {
    int success;

    glGetProgramiv(shaderProgram, GL_LINK_STATUS, &success);

    if (!success) {
        const unsigned int size_infoLog{512};
        char infoLog[size_infoLog];

        glGetProgramInfoLog(shaderProgram, size_infoLog, nullptr, infoLog);
        throw Shader::ErrorShader{std::string{"Une erreur est survenue lors du linkage des shaders :\n"} + infoLog};
    }
}


GLuint create_shaderProgram(const std::string &vertexPath, const std::string &fragmentPath) {
    const GLuint vertexShader = createShader(vertexPath, GL_VERTEX_SHADER);
    const GLuint fragmentShader = createShader(fragmentPath, GL_FRAGMENT_SHADER);

    const GLuint shaderProgram = glCreateProgram();
    glAttachShader(shaderProgram, vertexShader);
    glAttachShader(shaderProgram, fragmentShader);
    glLinkProgram(shaderProgram);
    check_linkShaders(shaderProgram);

    glDeleteShader(fragmentShader);
    glDeleteShader(vertexShader);

    return shaderProgram;
}

// -------------------------------------------------------------------------------//

} // anonymous namespace

Shader::Shader(const std::string &vertexPath, const std::string &fragmentPath) {
    ID = create_shaderProgram(vertexPath, fragmentPath);
}


Shader::~Shader() {
    glDeleteProgram(ID);
}


/* -------------  Set Uniforms --------------------------------------*/

template<>
void Shader::setUniform(const std::string &name, const int &value) {
    const int locationUniform = getLocationUniform(name);
    glUniform1i(locationUniform, value);
}


template<>
void Shader::setUniform(const std::string &name, const bool &value) {
    setUniform<int>(name, value);
}


template<>
void Shader::setUniform(const std::string &name, const unsigned int &value) {
    const int locationUniform = getLocationUniform(name);
    glUniform1ui(locationUniform, value);
}


template<>
void Shader::setUniform(const std::string &name, const float &value) {
    const int locationUniform = getLocationUniform(name);
    glUniform1f(locationUniform, value);
}


template<>
void Shader::setUniform(const std::string &name, const Eigen::VectorXf &value) {
    const int locationUniform = getLocationUniform(name);
    const auto numElements = static_cast<GLsizei>(value.rows());
    glUniform1fv(locationUniform, numElements, value.data());
}


template<>
void Shader::setUniform(const std::string &name, const Eigen::Vector3f &value) {
    const int locationUniform = getLocationUniform(name);
    glUniform3f(locationUniform, value(0), value(1), value(2));
}


template<>
void Shader::setUniform(const std::string &name, const Eigen::Matrix4f &value) {
    const int locationUniform = getLocationUniform(name);
    glUniformMatrix4fv(locationUniform, 1, GL_FALSE, value.data());
}


template<>
void Shader::setUniform(const std::string &name,
                        const Eigen::Matrix<float, Eigen::Dynamic, 3, Eigen::RowMajor> &value) {
    const int locationUniform = getLocationUniform(name);
    const auto numElements = static_cast<GLsizei>(value.rows());
    glUniform3fv(locationUniform, numElements, value.data());
}


template<>
void Shader::setUniform (const std::string &name, float value1, float value2, float value3) {
    const int locationUniform = getLocationUniform(name);
    glUniform3f(locationUniform, value1, value2, value3);
}


Shader::ErrorShader::ErrorShader(const std::string &message_error):
    std::runtime_error{message_error} {}
