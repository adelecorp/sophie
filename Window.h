#pragma once

#include <iostream>

#include <glad/glad.h>
#include <GLFW/glfw3.h>


class Window {
public:

    Window(unsigned int width, unsigned int height, const char *title);
    ~Window();

    void update_deltaTime();

    inline GLFWwindow* get_glfwWindow() const { return window; }

    //---------------------

    class ErrorWindow : public std::runtime_error {
    public:
        explicit ErrorWindow(const std::string &message_error);
    };

private:
    /*--------------------*/
    /* Attributes */
    /*--------------------*/

    float lastFrame;
    GLFWwindow *window;

private:

    /*--------------------*/
    /* Functions */
    /*--------------------*/

    // Has to be done after the current context of Glfw is set (glfwMakeContextCurrent)
    void init_glad ();

    void init_glfwWindow();

    void createWindow (int width, int height, const char *title);
};
