cmake_minimum_required(VERSION 3.5.1)

set(APPLICATION Sophie)
project(${APPLICATION})

# -std=c++20
set(CMAKE_CXX_STANDARD 20)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall -pedantic -Wextra")


set(OpenGL_GL_PREFERENCE LEGACY)
find_package(OpenGL REQUIRED)

find_package(glfw3 3.3 REQUIRED STATIC)


add_library(glad "Dependencies/GLAD/glad/src/glad.c")

add_library(imgui STATIC
        Dependencies/imgui-1.89.3/imgui.cpp
        Dependencies/imgui-1.89.3/imgui_demo.cpp
        Dependencies/imgui-1.89.3/imgui_draw.cpp
        Dependencies/imgui-1.89.3/imgui_widgets.cpp
        Dependencies/imgui-1.89.3/imgui_tables.cpp
        Dependencies/imgui-1.89.3/misc/cpp/imgui_stdlib.cpp)


include_directories("Dependencies/imgui-1.89.3"
                    "Dependencies/GLAD/glad/include"
                    "Dependencies/eigen-3.4.0")

# Add Sources
set(SOURCE_FILES
        Dependencies/imgui-1.89.3/backends/imgui_impl_glfw.cpp
        Dependencies/imgui-1.89.3/backends/imgui_impl_glfw.h
        Dependencies/imgui-1.89.3/backends/imgui_impl_opengl3.cpp
        Dependencies/imgui-1.89.3/backends/imgui_impl_opengl3.h

        Dependencies/stb/stb_image.h
        definition_stbImage.h

        stylizations/Stylization.h stylizations/Stylization.cpp
        stylizations/Xtoon.h stylizations/Xtoon.cpp
        stylizations/Phong.h stylizations/Phong.cpp
        stylizations/SSAO.h stylizations/SSAO.cpp
        stylizations/MLAA.h stylizations/MLAA.cpp

        main.cpp
        Utility.cpp Utility.h
        Shader.cpp Shader.h
        Camera.cpp Camera.h
        Mesh.cpp Mesh.h
        HalfEdgeStructure.cpp HalfEdgeStructure.h   
        DeferredRenderer.cpp DeferredRenderer.h
        UserInterface.cpp UserInterface.h
        Window.cpp Window.h
        Renderer.cpp Renderer.h
        TextureImage.cpp TextureImage.h)

add_executable(${APPLICATION} ${SOURCE_FILES})

# Link libraries
target_link_libraries(${APPLICATION}
        ${OPENGL_gl_LIBRARY}
        glad glfw imgui)
