#include "TextureImage.h"

#include "definition_stbImage.h"

using namespace std;


TextureImage::TextureImage() :
    path_image{"../sophie/Textures_images/bar.jpg"}
{
    glGenTextures(1, &texture);

    loadImage();

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    glBindTexture(GL_TEXTURE_2D, 0);
}


TextureImage::~TextureImage() {
    glDeleteTextures(1, &texture);
}


void TextureImage::loadImage() {
    int width, height, numChannels;

    stbi_set_flip_vertically_on_load(true);
    unsigned char *image = stbi_load(path_image.c_str(), &width, &height, &numChannels, 0);
    if (!image) {
        throw ErrorTextureImage{"Could not read the image : " + path_image};
    }

    glBindTexture(GL_TEXTURE_2D, texture);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, image);

    stbi_image_free(image);
}


TextureImage::ErrorTextureImage::ErrorTextureImage(const std::string &message_error):
    std::runtime_error{message_error} {}
