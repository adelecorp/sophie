#pragma once

#include <glad/glad.h>


namespace Utility {
    void configureFrameBuffer(GLuint &FBO, GLuint &target, GLsizei width, GLsizei height,
                              unsigned int numCoefficients, GLint internalformat,
                              GLint textureFilter = GL_NEAREST,
                              GLint textureWrap = GL_CLAMP_TO_EDGE, GLenum type = GL_FLOAT);

    void renderScreenQuad(GLuint screenQuad_VAO);
}
