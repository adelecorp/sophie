#pragma once

#include "Camera.h"
#include "DeferredRenderer.h"
#include "Mesh.h"


class Renderer {
public:
    Renderer(unsigned int width, unsigned int height);
    ~Renderer();

    Camera* getCamera();

    void render();

    void user_interface();

    void updateProjection();
    void callbackWindowSize(unsigned int width, unsigned int height);

private:
    /*--------------------*/
    /* Attributes */
    /*--------------------*/

    Camera camera;
    DeferredRenderer *deferredRenderer;
    Mesh mesh;

    /*--------------------*/
    /* Functions */
    /*--------------------*/

    void draw();
};
