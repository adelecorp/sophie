#pragma once

#include "Renderer.h"
#include "Window.h"


class UserInterface {
public:
    UserInterface(Window &window, unsigned int width, unsigned int height);

    ~UserInterface();

    void renderLoop ();

    void print() const;

    static void getWindowSize (unsigned int &width, unsigned int &height);

private:
    /* ----------------------*/
    /* Attributes */
    /* ----------------------*/

    static Renderer *renderer;
    static Camera *camera;
    Window &window;

    // Frame Rate
    float accumulatedTime;
    unsigned int numDoneFrames;
    float framesPerSecond;

    static constexpr unsigned int NUM_SAVED_FRAMERATES = 100;
    float savedFrameRates[NUM_SAVED_FRAMERATES];
    unsigned int offsetArraySavedFrameRates;
    float accumulatedTimeForPlot;
    unsigned int numDoneFramesForPLot;

    /* ----------------------*/
    /* Functions */
    /* ----------------------*/

    // timeSinceLastFrame : Time elapsed since last frame, in seconds.
    void updateFrameRate(float timeSinceLastFrame);

    void set_callbacks (GLFWwindow *window);

    static void callback_frameBufferSize(GLFWwindow *, int new_width, int new_height);
    static void callback_mouse_pressedReleased (GLFWwindow *window, int button, int action, int);
    static void callback_mouseMovement(GLFWwindow *, double position_x, double position_y);
    static void callback_scroll (GLFWwindow *, double , double offset_y);
    static void callback_key(GLFWwindow *window, int key, int , int action, int);
    static void callback_arrowKeys(int key);

    static void update_size (unsigned int width, unsigned int height);
};
