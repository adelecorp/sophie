#include "Mesh.h"

#include <fstream>
#include <filesystem>
#include <thread>

#include <imgui.h>
#include <misc/cpp/imgui_stdlib.h>

#include <Eigen/Geometry>

#include "HalfEdgeStructure.h"


using namespace Eigen;

//----------------------------------------------------------------------------

Mesh::Mesh(const std::string &filePath) :
    filePath{filePath},
    newMeshLoading{false},
    newMeshToLoad{false},
    progressFraction{0.0f}
{
    read_file();
    compute_normals();

    glGenVertexArrays(1, &VAO);
    glGenBuffers(2, VBO);
    glGenBuffers(1, &EBO);

    glBindVertexArray(VAO);
    const GLsizei stride = 3 * sizeof(GLfloat);
    initialize_vbo(0, stride);
    initialize_vbo(1, stride);

    updateVAO();
    computeModelMatrix();
}


Mesh::~Mesh() {
    glDeleteVertexArrays(1, &VAO);
    glDeleteBuffers(2, VBO);
    glDeleteBuffers(1, &EBO);
}


void Mesh::update() {
    if (newMeshToLoad.load(std::memory_order_relaxed)) {
        updateVAO();
        computeModelMatrix();
        newMeshLoading = false;
        newMeshToLoad.store(false, std::memory_order_relaxed);
    }
}


void Mesh::getModelMatrix(Matrix4f &modelMatrixToSet) const {
    modelMatrixToSet = modelMatrix;
}


bool Mesh::read_file () {
    std::fstream file{filePath, std::fstream::in};
    bool file_exits = file.good();

    if (file_exits) {
        unsigned int num_vertices, num_faces, num_verticesForFace;
        std::string line;

        progressFraction.store(0.0f, std::memory_order_relaxed);

        // First line : OFF
        std::getline(file, line);

        // Second line
        std::getline(file, line);
        std::stringstream streamLine{line};
        streamLine >> num_vertices >> num_faces;

        vertices.resize(num_vertices, 3);
        faces.resize(num_faces, 3);

        const float progressStep = 0.5f / (num_vertices + num_faces);

        for (unsigned int i = 0; i < num_vertices; ++i) {
            file >> vertices(i, 0) >> vertices(i, 1) >> vertices(i, 2);
            progressFraction.fetch_add(progressStep, std::memory_order_relaxed);
        }

        for (unsigned int i = 0; i < num_faces; ++i) {
            file >> num_verticesForFace >> faces(i, 0) >> faces(i, 1) >> faces(i, 2);
            progressFraction.fetch_add(progressStep, std::memory_order_relaxed);
        }

        file.close();
    }
    return file_exits;
}


/*
#include <iostream>
void Mesh::subdivideWith_loop(unsigned int level) {
    const auto num_vertices = vertices.rows();
    const HalfEdgeStructure structure{faces};

    for (long i = 0; i < num_vertices; ++i) {
        auto neighbors = structure.get_neighbors(i);

        std::cout << "Pour : " << i << std::endl;
        for (unsigned int neighbor : neighbors)
            std::cout << neighbor << " ";

        std::cout << std::endl;
    }

    std::cout << "ok" << std::endl;
}
*/


void Mesh::computeNormals(float progressStep, std::mutex mutexes[], unsigned int rangeMutexes,
                          unsigned int rangeBegin, unsigned int rangeEnd) {
    for (unsigned int i = rangeBegin; i < rangeEnd; ++i) {
        for (unsigned int j = 0; j < 3; ++j) {
            const unsigned int vertex = faces(i, j);
            const unsigned int opposedVertex1 = faces(i, (j + 1) % 3);
            const unsigned int opposedVertex2 = faces(i, (j + 2) % 3);
            const Vector3f edge1 = vertices.row(opposedVertex1) - vertices.row(vertex);
            const Vector3f edge2 = vertices.row(opposedVertex2) - vertices.row(vertex);
            const Vector3f normal = edge1.cross(edge2);

            const unsigned int indexMutex = vertex / rangeMutexes;
            mutexes[indexMutex].lock();
            normals.row(vertex) += normal;
            mutexes[indexMutex].unlock();
        }

        progressFraction.fetch_add(progressStep, std::memory_order_relaxed);
    }
}


void Mesh::compute_normals() {
    const unsigned int numVertices = vertices.rows();
    normals = MatrixX3fR::Zero(numVertices, 3);
    const auto numFaces = faces.rows();
    const float progressStep = 0.5f / numFaces;

    const unsigned int rangeMutexes = 100;
    const unsigned int numMutexes = ceil((float)numVertices / rangeMutexes);
    auto mutexes = new std::mutex[numMutexes];

    auto numThreads = std::thread::hardware_concurrency();
    if (numThreads == 0) {
        numThreads = 1;
    }

    const auto division = std::div(numFaces, numThreads);
    const unsigned int slicePerThread = division.quot;
    const unsigned int slicePerThreadRemainder = division.rem;

    auto threads = new std::thread[numThreads];
    unsigned int rangeBegin = 0;
    for (unsigned int i = 0; i < numThreads; ++i) {
        unsigned int rangeEnd = rangeBegin + slicePerThread;
        if (i < slicePerThreadRemainder) {
            rangeEnd += 1;
        }
        threads[i] = std::thread(&Mesh::computeNormals, this, progressStep, mutexes,
                                 rangeMutexes, rangeBegin, rangeEnd);
        rangeBegin = rangeEnd;
    }

    for (unsigned int i = 0; i < numThreads; ++i) {
        threads[i].join();
    }

    delete[] threads;
    delete[] mutexes;

    normals.rowwise().normalize();
}


void Mesh::draw() const {
    glBindVertexArray(VAO);
    glDrawElements(GL_TRIANGLES, GLsizei(numFaces), GL_UNSIGNED_INT, nullptr);
    glBindVertexArray(0);
}


void Mesh::updateVAO() {
    glBindVertexArray(VAO);

    write_vbo(0, vertices);
    write_vbo(1, normals);

    // Faces
    numFaces = faces.size();
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, numFaces * sizeof(GLuint), faces.data(), GL_DYNAMIC_DRAW);

    glBindVertexArray(0);
}


void Mesh::write_vbo (unsigned int index_attribute, const MatrixX3fR &attributes) {
    glBindBuffer(GL_ARRAY_BUFFER, VBO[index_attribute]);
    glBufferData(GL_ARRAY_BUFFER, attributes.size() * sizeof(GLfloat), attributes.data(), GL_DYNAMIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
}


void Mesh::initialize_vbo(unsigned int index_attribute, GLsizei stride) {
    glBindBuffer(GL_ARRAY_BUFFER, VBO[index_attribute]);
    glVertexAttribPointer(index_attribute, 3, GL_FLOAT, GL_FALSE, stride, nullptr);
    glEnableVertexAttribArray(index_attribute);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
}


void Mesh::computeModelMatrix() {
    const auto vertices_colwise = vertices.colwise();

    const auto vertex_min = vertices_colwise.minCoeff();
    const auto vertex_max = vertices_colwise.maxCoeff();

    const Vector3f centre = (vertex_min + vertex_max) * 0.5f;

    const auto difference = vertex_max - vertex_min;
    const float maxDiameter = difference.norm();

    const float maxDiameterInScene = 2.0f;

    Affine3f transform(Eigen::Scaling(maxDiameterInScene / maxDiameter));
    transform.translate(-centre);
    modelMatrix = transform.matrix();
}


void Mesh::loadMesh() {
    if (read_file()) {
        compute_normals();
        newMeshToLoad.store(true, std::memory_order_relaxed);
    }
}


void Mesh::user_interface() {
    if (newMeshLoading) {
        const float progression = progressFraction.load(std::memory_order_relaxed);
        ImGui::ProgressBar(progression, ImVec2(-1, 0), "Loading file");
    } else {
        if (ImGui::InputText("Mesh File", &filePath)) {
            const bool fileExists = std::filesystem::exists(filePath);
            if (fileExists) {
                newMeshLoading = true;
                auto thread = std::thread(&Mesh::loadMesh, this);
                thread.detach();
            }
        }
    }
}

Mesh::ErrorMesh::ErrorMesh(const std::string &message_error):
    std::runtime_error{message_error} {}
