#include "HalfEdgeStructure.h"



using namespace std;
using namespace Eigen;

/* Definitions */

struct HalfEdgeStructure::Vertex {
    unsigned int index;
    HalfEdge *oneHalfEdge;
};

struct HalfEdgeStructure::HalfEdge {
   Vertex *origin_vertex;
   HalfEdge *next;
   HalfEdge *prev;
   HalfEdge *pair;
};


//---------------------------------------------------

HalfEdgeStructure::HalfEdgeStructure(const MatrixX3uiR &faces) {
    MapHalfEdges mapHalfEdges;
    const auto num_faces = faces.rows();
    const auto num_verticesFace = faces.cols();
    const auto num_vertices = num_verticesFace * num_faces;

    vertices.resize((unsigned long) num_vertices);
    halfEdges = new HalfEdge[num_vertices];

    // Init vertices with vertex indices
    unsigned int index = 0;

    for (Vertex &vertex : vertices) {
        vertex.index= index;
        ++index;
    }

    //-----

    for (long index_face = 0; index_face < num_faces; ++index_face) {
        const auto face = faces.row(index_face);
        const auto firstHalfEdge_face = index_face * num_verticesFace;

        for (unsigned int i = 0; i < num_verticesFace; ++i) {
            const auto index_halfEdge = firstHalfEdge_face + i;
            // Initialize HalfEdge
            const unsigned int index_originVertex = face(i);
            const unsigned int index_endVertex = face((i + 1) % num_verticesFace);
            const auto next_halfEdge = firstHalfEdge_face + ((i + 1) % num_verticesFace);
            const auto previous_halfEdge = firstHalfEdge_face + ((i + 2) % num_verticesFace);

            auto originVertex = &vertices.at(index_originVertex);
            HalfEdge &current_HalfEdge = halfEdges[index_halfEdge];

            current_HalfEdge.origin_vertex = originVertex;
            current_HalfEdge.next = &halfEdges[next_halfEdge];
            current_HalfEdge.prev = &halfEdges[previous_halfEdge];

            auto halfEdgePair_edge = make_pair(index_endVertex, index_originVertex);
            if (mapHalfEdges.count(halfEdgePair_edge) > 0) {
                HalfEdge *halfEdgePair = mapHalfEdges[halfEdgePair_edge];
                current_HalfEdge.pair = halfEdgePair;
                halfEdgePair->pair = &current_HalfEdge;
            }

            // Assign to one vertex
            // TODO : test à null
            if (originVertex->oneHalfEdge == nullptr) {
                originVertex->oneHalfEdge = &current_HalfEdge;
            }

            // Stock in halfEdge Map
            halfEdgePair_edge.first = index_originVertex;
            halfEdgePair_edge.second = index_endVertex;
            mapHalfEdges[halfEdgePair_edge] = &current_HalfEdge;
        }
    }
}


HalfEdgeStructure::~HalfEdgeStructure() {
    delete [] halfEdges;
}


vector<unsigned int> HalfEdgeStructure::get_neighbors(long index_vertex) const {
    vector<unsigned int> neighbors;
    const Vertex &vertex = vertices.at(index_vertex);
    HalfEdge *start_halfEdge = vertex.oneHalfEdge;
    HalfEdge **current_halfEdge = &start_halfEdge;

    do {
        current_halfEdge = &(*current_halfEdge)->next;

        const Vertex *neighbor = (*current_halfEdge)->origin_vertex;
        neighbors.push_back(neighbor->index);

        current_halfEdge = &(*current_halfEdge)->next;
        current_halfEdge = &(*current_halfEdge)->pair;
    } while (*current_halfEdge != start_halfEdge);

    return neighbors;
}
