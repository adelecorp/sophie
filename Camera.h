#pragma once

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <Eigen/Core>
#include <Eigen/Geometry>


class Camera {
public:
    /* Definition */

    // Used as abstraction to stay away from window-system specific input methods
    enum Movement {
        FORWARD,
        BACKWARD,
        LEFT,
        RIGHT,
        BREAK
    };

    /*-------------------------------------------------------------*/

    explicit Camera();

    void update_printParameters (const Eigen::Vector3f &centre, float new_maxDiameter);
    Eigen::Matrix4f update_view (float deltaTime);

    // aspectRatio : width / height
    void updateAspectRatio(float aspectRatio);

    Eigen::Matrix4f get_projection() const;

    void process_keyboard (Movement direction, float deltaTime);
    void process_leftMouse_pressed (GLFWwindow *window, int window_width, int window_height);
    void process_mouseMovement(float mouseX, float mouseY, int window_width, int window_height);
    void process_leftMouse_released(GLFWwindow *window);
    void processMouseScroll(double offset_y);

private:

    enum State {
        REGULAR_ROTATION,
        USER_ROTATION,
        NO_ROTATION
    } state;

    State previousState;

    /*-------------*/
    /* Attributes */
    /*-------------*/

    Eigen::Vector3f position;
    Eigen::Vector3f target;
    Eigen::Vector3f front;
    Eigen::Vector3f right;
    Eigen::Vector3f worldUp;
    Eigen::Matrix4f projection;

    // Arcball
    bool first_entered_mouse;
    float radius;
    float velocity;
    Eigen::Vector3f axis_regularRotation;
    Eigen::Vector3f previousMousePosition;
    Eigen::Quaternionf currentRotation;
    Eigen::Quaternionf newRotation;

    const float movementSpeed;
    const float zoom_min, zoom_max;

    float aspectRatio;
    float zoom;

    /*-------------*/
    /* Functions */
    /*-------------*/

    void update();

    Eigen::Matrix4f compute_lookAt ();
    void update_projection();

    Eigen::Vector3f compute_mouseDirection (float mouseX, float mouseY, int window_width, int window_height);
    void process_break();

    void regular_rotate (float deltaTime);
    void rotate (float mouseX, float mouseY, int window_width, int window_height);
    void rotate(const Eigen::Quaternionf &rotation);
};

