#include "Renderer.h"

#include <imgui.h>

using namespace Eigen;

//-------------------------------------------------------------------------------------------------------------


Renderer::Renderer(unsigned int width, unsigned int height) :
    mesh{"../sophie/Meshes/bunny.off"}
{
    deferredRenderer = new DeferredRenderer{width, height};
    // Update projection matrix in the Deferred Renderer
    const float aspectRatio = static_cast<float>(width) / height;
    camera.updateAspectRatio(aspectRatio);
    const auto projection = camera.get_projection();
    deferredRenderer->update_projection(projection);
}


Renderer::~Renderer() {
    delete deferredRenderer;
}


Camera *Renderer::getCamera()
{
    return &camera;
}


void Renderer::draw () {
    deferredRenderer->geometry_pass(mesh);
    deferredRenderer->lightingPass();
}


void Renderer::render () {
    ImGuiIO& io = ImGui::GetIO();

    mesh.update();
    Eigen::Matrix4f modelMatrix;
    mesh.getModelMatrix(modelMatrix);
    const auto view = camera.update_view(io.DeltaTime);
    deferredRenderer->update_viewAndModel(view, modelMatrix);

    draw();
}


void Renderer::user_interface() {
    mesh.user_interface();
    const auto projection = camera.get_projection();
    deferredRenderer->user_interface(projection);
}


void Renderer::updateProjection() {
    const auto projection = camera.get_projection();
    deferredRenderer->update_projection(projection);
}


void Renderer::callbackWindowSize(unsigned int width, unsigned int height) {
    const float aspectRatio = static_cast<float>(width) / height;
    camera.updateAspectRatio(aspectRatio);
    const auto projection = camera.get_projection();
    deferredRenderer->update_projection(projection);
    deferredRenderer->callback_frameBufferSize(width, height);
}
