#include "Camera.h"


constexpr float DEGREE_TO_RADIANS_FACTOR = float(M_PI) / 180.0f;

using namespace Eigen;

//--------------------------------------------------------------------------

Camera::Camera():
    state{REGULAR_ROTATION},
    previousState{state},
    target{Vector3f::Zero()},
    front{-Eigen::Vector3f::UnitZ()},
    worldUp{Eigen::Vector3f::UnitY()},
    projection{Matrix4f::Zero()},
    first_entered_mouse{true},
    radius{2.0f * 1.5f},
    velocity{1.0f},
    axis_regularRotation{Eigen::Vector3f::UnitY()},
    currentRotation{AngleAxisf{0.0f, axis_regularRotation}},
    movementSpeed{0.25f},
    zoom_min{1.0f},
    zoom_max{45.0f},
    aspectRatio{0.0f},
    zoom{15.0f}
{
    update();
}


void Camera::process_keyboard(Camera::Movement direction, float deltaTime) {
    const float velocity = movementSpeed * deltaTime;

    switch (direction) {
        case FORWARD:
            //target += worldUp * velocity;
            radius -= velocity;
        break;
        case BACKWARD:
            //target -= worldUp * velocity;
            radius += velocity;
        break;
        case LEFT:
            target -= right * velocity;
        break;
        case RIGHT:
            target += right * velocity;
        break;
        case BREAK:
            process_break();
        break;
    }
}


void Camera::process_break() {
    switch (state) {
        case NO_ROTATION:
            state = REGULAR_ROTATION;
        break;
        case REGULAR_ROTATION:
            state = NO_ROTATION;
        break;
        case USER_ROTATION:
            // Do nothing
        break;
    }
}


Vector3f Camera::compute_mouseDirection (float mouseX, float mouseY, int window_width, int window_height) {
    const float maxRange = std::min(window_width, window_height) - 1.0f;

    const float x = (2.0f * mouseX - window_width + 1.0f) / maxRange;
    const float y = (2.0f * mouseY - window_height + 1.0f) / maxRange;
    const float x2Plusy2 = x * x + y * y;

    float z = 0.0f;
    if (x2Plusy2 <= 0.5f) {
        z = std::sqrt(1.0f - x2Plusy2);
    } else {
        z = 0.5f / std::sqrt(x2Plusy2);
    }

    const Vector3f mouseDirection{x, -y, z};
    return mouseDirection.normalized();
}


void Camera::regular_rotate (float deltaTime) {
    const float rotationAngle = float(M_PI) * 0.25f * deltaTime * velocity;
    const Quaternionf rotation{AngleAxisf{rotationAngle, axis_regularRotation}};
    currentRotation *= rotation;
    rotate(currentRotation);
}


void Camera::rotate (float mouseX, float mouseY, int window_width, int window_height) {
    const Vector3f middleDirection{0.0f, 0.0f, 1.0f};
    const Vector3f mouseDirection = compute_mouseDirection(mouseX, mouseY, window_width, window_height);

    const Vector3f direction = mouseDirection - previousMousePosition;
    previousMousePosition = mouseDirection;
    velocity = direction.norm() * 100.0f;

    if (velocity > 0.01f) {
        const Quaternionf rotation = Quaternionf::FromTwoVectors(mouseDirection, middleDirection);
        newRotation = currentRotation * rotation;
        rotate(newRotation);

        axis_regularRotation = mouseDirection.cross(middleDirection);
        axis_regularRotation.normalize();
    }
}


void Camera::rotate(const Quaternionf &rotation) {
    front = rotation * -Eigen::Vector3f::UnitZ();
    worldUp = rotation * Eigen::Vector3f::UnitY();
    update();
}


void Camera::process_leftMouse_pressed (GLFWwindow *window, int window_width, int window_height) {
    switch (state) {
    case NO_ROTATION:
    case REGULAR_ROTATION: {
            glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
            glfwSetCursorPos(window, 0.5 * (window_width - 1.0), 0.5 * (window_height - 1.0));
            previousMousePosition << 0.0f, 0.0f, 1.0f;
            newRotation = currentRotation;
            previousState = state;
            state = USER_ROTATION;
    } break;
    case USER_ROTATION:
        // Impossible
    break;
    }
}


void Camera::process_mouseMovement(float mouseX, float mouseY, int window_width, int window_height) {
    switch (state) {
        case NO_ROTATION:
        case REGULAR_ROTATION:
            // Do nothing
            break;
        case USER_ROTATION:
            rotate(mouseX, mouseY, window_width, window_height);
            break;
    }
}


void Camera::process_leftMouse_released(GLFWwindow *window) {
    switch (state) {
        case NO_ROTATION:
        case REGULAR_ROTATION:
            // Impossible
            break;
        case USER_ROTATION:
            currentRotation = newRotation;
            state = previousState;
            glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
            break;
    }
}


void Camera::processMouseScroll(double offset_y) {
    if (zoom >= zoom_min && zoom <= zoom_max) {
        zoom -= static_cast<float>(offset_y);
    }

    zoom = std::max(zoom, zoom_min);
    zoom = std::min(zoom, zoom_max);

    update_projection();
}


Matrix4f Camera::update_view(float deltaTime) {
    switch (state) {
        case NO_ROTATION:
            update();
        break;
        case REGULAR_ROTATION:
            regular_rotate(deltaTime);
        break;
        case USER_ROTATION:
            // Do nothing
        break;
    }

    return compute_lookAt();
}


Matrix4f Camera::get_projection() const {
    return projection;
}


void Camera::update() {
    position = target - front * radius;
    right = front.cross(worldUp);
    right.normalize();
}


void Camera::updateAspectRatio(float newAspectRatio) {
    aspectRatio = newAspectRatio;
    update_projection();
}


Matrix4f Camera::compute_lookAt () {
    const Vector3f front_vector = position + front;
    const RowVector3f invDirection = (position - front_vector).normalized();

    RowVector3f right = worldUp.cross(invDirection);
    right.normalize();

    const RowVector3f up = invDirection.cross(right);

    const float translation_x = right.dot(-position);
    const float translation_y = up.dot(-position);
    const float translation_z = invDirection.dot(-position);

    Matrix4f lookAt;
    lookAt << right,        translation_x,
              up,           translation_y,
              invDirection, translation_z,
              0.0f, 0.0f, 0.0f, 1.0f;

    return lookAt;
}


void Camera::update_projection() {
    assert(aspectRatio > 0.0f);

    //nearPlane, farPlane : distance of the near and far plane of the frustrum
    const float nearPlane = 0.1f;
    const float farPlane = 1000.0f;

    const float scale = tanf(zoom * DEGREE_TO_RADIANS_FACTOR) * nearPlane;
    const float rangePlane = farPlane - nearPlane;

    projection(0,0) = nearPlane / (aspectRatio * scale);
    projection(1,1) = nearPlane / scale;

    projection(2,2) = -(farPlane + nearPlane) / rangePlane;
    projection(2,3) = -1.0f;

    projection(3,2) = -2.0f * farPlane * nearPlane / rangePlane;

    projection.transposeInPlace();
}
