#pragma once


#include <glad/glad.h>

#include <string>
#include <stdexcept>


class TextureImage {
public:
    TextureImage();
    ~TextureImage();

    inline void bind() const {
        glBindTexture(GL_TEXTURE_2D, texture);
    }

private:
    /* Attributes */

    std::string path_image;
    unsigned int texture;

    // ---------------------------------------

    class ErrorTextureImage : public std::runtime_error {
    public:
        ErrorTextureImage(const std::string &message_error);
    };

    /* Functions */

    void loadImage();
};
