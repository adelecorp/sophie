#include "MLAA.h"

#include <imgui.h>
#include <fstream>

#include "../Utility.h"


MLAA::MLAA(unsigned int width, unsigned int height, const GLuint &screenQuad_VAO,
           GLuint aliasedTarget):
    isActivated{true},
    threshold{0.1f},
    aliasedTarget{aliasedTarget},
    luminanceShader{"simplePositionTexture.vert.glsl", "mlaa/luminance.frag.glsl"},
    edgesShader{"simplePositionTexture.vert.glsl", "mlaa/edges.frag.glsl"},
    weightsShader{"simplePositionTexture.vert.glsl", "mlaa/weights.frag.glsl"},
    areaMap{0},
    maxEdgeLength{9},
    blendingShader{"simplePositionTexture.vert.glsl", "mlaa/blending.frag.glsl"},
    screenQuad_VAO{screenQuad_VAO}
{
    Utility::configureFrameBuffer(luminanceFramebuffer, luminanceTarget, width, height, 1, GL_RED);
    Utility::configureFrameBuffer(edgesFramebuffer, edgesTarget, width, height,
                                  2, GL_RG8, GL_LINEAR, GL_MIRRORED_REPEAT, GL_UNSIGNED_BYTE);
    Utility::configureFrameBuffer(weightsFramebuffer, weightsTarget, width, height, 4, GL_RGBA32F);

    loadAreaMap();

    luminanceShader.activate();
    luminanceShader.setUniform("inputTexture", 0);

    edgesShader.activate();
    edgesShader.setUniform("luminanceTexture", 0);
    edgesShader.setUniform("threshold", threshold);

    weightsShader.activate();
    weightsShader.setUniform("areaMap", 0);
    weightsShader.setUniform("edgesTexture", 1);
    weightsShader.setUniform("maxEdgeLength", maxEdgeLength);

    blendingShader.activate();
    blendingShader.setUniform("aliasedTexture", 0);
    blendingShader.setUniform("weightTexture", 1);

    Shader::deactivate();
}


MLAA::~MLAA() {
    glDeleteFramebuffers(1, &luminanceFramebuffer);
    glDeleteTextures(1, &luminanceTarget);

    glDeleteFramebuffers(1, &edgesFramebuffer);
    glDeleteTextures(1, &edgesTarget);

    glDeleteFramebuffers(1, &weightsFramebuffer);
    glDeleteTextures(1, &weightsTarget);

    if (areaMap) {
        glDeleteTextures(1, &areaMap);
    }
}


bool MLAA::getIsActivated() const {
    return isActivated;
}


void MLAA::callback_frameBufferSize(unsigned int width, unsigned int height) const {
    if (isActivated) {
        glBindTexture(GL_TEXTURE_2D, luminanceTarget);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RED, width, height, 0, GL_RED, GL_FLOAT, nullptr);

        glBindTexture(GL_TEXTURE_2D, edgesTarget);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RG8, width, height, 0, GL_RG, GL_UNSIGNED_BYTE, nullptr);

        glBindTexture(GL_TEXTURE_2D, weightsTarget);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, width, height, 0, GL_RGBA, GL_FLOAT, nullptr);

        glBindTexture(GL_TEXTURE_2D, 0);
    }
}


void MLAA::passes() const {
    if (isActivated) {
        luminancePass();
        edgesPass();
        weightsPass();
        blendingPass();
        Shader::deactivate();
    }
}


void MLAA::luminancePass() const {
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, aliasedTarget);

    glBindFramebuffer(GL_FRAMEBUFFER, luminanceFramebuffer);
    luminanceShader.activate();
    Utility::renderScreenQuad(screenQuad_VAO);
}


void MLAA::edgesPass() const {
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, luminanceTarget);

    glBindFramebuffer(GL_FRAMEBUFFER, edgesFramebuffer);
    edgesShader.activate();
    Utility::renderScreenQuad(screenQuad_VAO);
}


void MLAA::weightsPass() const {
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, areaMap);

    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D, edgesTarget);

    glBindFramebuffer(GL_FRAMEBUFFER, weightsFramebuffer);
    weightsShader.activate();
    Utility::renderScreenQuad(screenQuad_VAO);
}


void MLAA::blendingPass() const {
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, aliasedTarget);

    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D, weightsTarget);

    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    blendingShader.activate();
    Utility::renderScreenQuad(screenQuad_VAO);
}


void MLAA::loadAreaMap() {
    std::string areaPath = "../sophie/Textures_images/areas_";
    areaPath += std::to_string(maxEdgeLength) + ".bin";
    std::ifstream file{areaPath, std::ifstream::binary};
    const bool file_exits = file.good();

    if (file_exits) {        
        const unsigned int numAreas = 5 * maxEdgeLength * 5 * maxEdgeLength * 2;

        auto areas = new float[numAreas];
        for(unsigned int i = 0; i < numAreas; i++) {
            file.read((char *)&areas[i], sizeof(float));
        }

        file.close();

        configureAreaMapTexture(areas);
        delete[] areas;
    }
}


void MLAA::configureAreaMapTexture(float areas[]) {
    glGenTextures(1, &areaMap);
    glBindTexture(GL_TEXTURE_2D, areaMap);

    const unsigned int imageSize = 5 * maxEdgeLength;
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RG32F, imageSize, imageSize, 0, GL_RG, GL_FLOAT, areas);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

    glBindTexture(GL_TEXTURE_2D, 0);
}


void MLAA::user_interface() {
    if (ImGui::CollapsingHeader("MLAA")) {
        ImGui::Checkbox("Activate##MLAA", &isActivated);

        if (isActivated) {
            if (ImGui::DragFloat("minimum difference", &threshold, 0.0001f, 0.0f, 1.0f, "%.4f")) {
                edgesShader.activate();
                edgesShader.setUniform("threshold", threshold);
                Shader::deactivate();
            }
        }
    }
}
