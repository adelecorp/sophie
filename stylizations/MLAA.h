#pragma once

#include "../Shader.h"

class MLAA {
public:
    MLAA(unsigned int width, unsigned int height, const GLuint &screenQuad_VAO,
         GLuint aliasedTarget);
    ~MLAA();

    bool getIsActivated() const;

    void passes() const;

    void callback_frameBufferSize(unsigned int width, unsigned int height) const;

    void user_interface();

private:
    /*----------------*/
    /* Attributes */
    /*----------------*/
    bool isActivated;
    float threshold;

    const GLuint aliasedTarget;

    Shader luminanceShader;
    GLuint luminanceFramebuffer;
    GLuint luminanceTarget;

    Shader edgesShader;
    GLuint edgesFramebuffer;
    GLuint edgesTarget;

    Shader weightsShader;
    GLuint weightsFramebuffer;
    GLuint weightsTarget;
    GLuint areaMap;
    const unsigned int maxEdgeLength;

    Shader blendingShader;

    const GLuint &screenQuad_VAO;

    /*----------------*/
    /* Methods */
    /*----------------*/
    void loadAreaMap();
    void configureAreaMapTexture(float areas[]);

    void luminancePass() const;
    void edgesPass() const;
    void weightsPass() const;
    void blendingPass() const;
};
