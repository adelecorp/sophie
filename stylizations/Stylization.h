#pragma once

#include <Eigen/Core>


class Stylization {
public:
    Stylization();
    virtual ~Stylization();

    virtual void activateGeometryShader() const = 0;
    virtual void activateLightingShader() = 0;

    virtual void updateViewAndModel(const Eigen::Matrix4f &view, const Eigen::Matrix4f &model) = 0;
    virtual void updateProjection(const Eigen::Matrix4f &projection) = 0;

    virtual void bindNeededTextures_lighting() const;

    virtual unsigned int neededGeometryAttributes () const;

    virtual void user_interface();
};
