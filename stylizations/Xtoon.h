#pragma once

#include "Stylization.h"
#include "../Shader.h"
#include "../TextureImage.h"

#include "SSAO.h"


class XToon : public Stylization {
public:
    XToon(SSAO *ssao);
    ~XToon() override;

    void user_interface() override;

    void bindNeededTextures_lighting() const override;

    unsigned int neededGeometryAttributes () const override;

    void updateViewAndModel(const Eigen::Matrix4f &view, const Eigen::Matrix4f &model) override;
    void updateProjection(const Eigen::Matrix4f &projection) override;

    void activateGeometryShader() const override;
    void activateLightingShader() override;

private:
    /* Attributes */

    bool depthBased;

    Shader geometryPass, lightingPass;

    TextureImage xToon_map;
    const Eigen::Vector4f lightPosition;

    float effectMagnitude;

    SSAO *const ssao;

    /* Methods */

    void changeMode();
};
