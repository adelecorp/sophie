#pragma once

#include "../Shader.h"
#include <Eigen/Core>
#include <random>


class SSAO {
public:

    SSAO(unsigned int width, unsigned int height, const GLuint &screenQuad_VAO);

    ~SSAO();

    bool getIsActivated() const;

    void setDepthBuffer(GLuint depthBuffer);
    void setNormalBuffer(unsigned int attributeNormals);

    void user_interface(const Eigen::Matrix4f &projection);

    void activate_blurBuffer() const;

    void callback_frameBufferSize(unsigned int width, unsigned int height) const;

    void update_projection (const Eigen::Matrix4f &projection);

    void preLightingPass();

private:
    /*----------------*/
    /* Attributes */
    /*----------------*/

    bool isActivated;

    unsigned int num_samples;
    unsigned int size_noise;

    float hemisphere_radius;
    float tolerance_depth;
    float increment;
    unsigned int power;
    unsigned int sizeBlurFilter;

    Shader shader;
    Shader shader_blur;

    GLuint FBO, FBOBlur;
    GLuint ssaoTarget, ssaoBlurTarget;
    GLuint noiseTexture;

    const GLuint &screenQuad_VAO;
    unsigned int attributeNormals;
    GLuint depthBuffer;

    /*----------------*/
    /* Functions */
    /*----------------*/

    void pass() const;
    void pass_blur();

    void generate_noiseTexture (std::default_random_engine &generator, std::uniform_real_distribution<float> &random);
    Eigen::Matrix<float, Eigen::Dynamic, 3, Eigen::RowMajor> create_noise (std::default_random_engine &generator, std::uniform_real_distribution<float> &random);

    void write_samples (std::default_random_engine &generator, std::uniform_real_distribution<float> &random);

    float linear_interpolation (float a, float b, float delta) const;

    void writeGaussianKernel();

    void update_numSamples();
    void update_sizeNoise();
};
