#pragma once

#include "Stylization.h"
#include "../Shader.h"

#include "SSAO.h"


class Phong : public Stylization {
public:
    Phong(SSAO *ssao);
    ~Phong() override;

    inline void activateGeometryShader() const override { geometryPass.activate(); }
    void activateLightingShader() override;

    void bindNeededTextures_lighting() const override;

    void updateViewAndModel(const Eigen::Matrix4f &view, const Eigen::Matrix4f &model) override;
    void updateProjection(const Eigen::Matrix4f &projection) override;

private:
    /* Attributes */
    Shader geometryPass, lightingPass;
    const Eigen::Vector4f lightPosition;
    SSAO *const ssao;
};
