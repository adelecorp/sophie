#include "SSAO.h"

#include <Eigen/Core>
#include <imgui.h>

#include "../Utility.h"
#include "../UserInterface.h"



using namespace std;
using namespace Eigen;

static constexpr float PI = static_cast<float>(M_PI);

enum FILTER_DIRECTION {
    HORIZONTAL, VERTICAL
};

//---------------------------------------------------------------------------


SSAO::SSAO(unsigned int width, unsigned int height, const GLuint &screenQuad_VAO):
    isActivated{true},
    num_samples{32},
    size_noise{4},
    hemisphere_radius{0.300f},
    tolerance_depth{0.060f},
    increment{0.0001f},
    power{3},
    sizeBlurFilter{9},
    shader{"simplePositionTexture.vert.glsl", "ssao.frag.glsl"},
    shader_blur{"simplePositionTexture.vert.glsl", "gaussianFilter.frag.glsl"},
    screenQuad_VAO{screenQuad_VAO}
{
    const auto widthGLsizei = static_cast<GLsizei>(width);
    const auto heightGLsizei = static_cast<GLsizei>(height);
    Utility::configureFrameBuffer(FBO, ssaoTarget, widthGLsizei, heightGLsizei, 1, GL_RED);
    Utility::configureFrameBuffer(FBOBlur, ssaoBlurTarget, widthGLsizei, heightGLsizei, 1, GL_RED);

    uniform_real_distribution<float> random(0.0f, 1.0f);
    default_random_engine generator;

    generate_noiseTexture(generator, random);

    // Init SSAO Pass Uniforms
    shader.activate();
    shader.setUniform("num_samples", num_samples);
    shader.setUniform("hemisphere_radius", hemisphere_radius);
    shader.setUniform("tolerance_depth", tolerance_depth);
    shader.setUniform("increment", increment);

    shader.setUniform("depthBuffer", 0);
    shader.setUniform("mapViewNormals", 1);
    shader.setUniform("buffer_noise", 2);

    write_samples(generator, random);

    // Init SSAO Blur Pass Uniforms
    shader_blur.activate();
    shader_blur.setUniform("textureToFilter", 0);
    shader_blur.setUniform("power", power);
    shader_blur.setUniform("size_filter", sizeBlurFilter);
    writeGaussianKernel();

    Shader::deactivate();
}


SSAO::~SSAO() {
    glDeleteFramebuffers(1, &FBO);
    glDeleteFramebuffers(1, &FBOBlur);
    glDeleteTextures(1, &ssaoTarget);
    glDeleteTextures(1, &ssaoBlurTarget);
    glDeleteTextures(1, &noiseTexture);
}


bool SSAO::getIsActivated() const {
    return isActivated;
}


void SSAO::activate_blurBuffer () const {
    glBindTexture(GL_TEXTURE_2D, ssaoTarget);
}


/*----------------------- Create SSAO ----------------------------------------------*/

void SSAO::generate_noiseTexture (default_random_engine &generator, uniform_real_distribution<float> &random) {
    const auto ssaoNoise = create_noise(generator, random);
    glGenTextures(1, &noiseTexture);
    glBindTexture(GL_TEXTURE_2D, noiseTexture);
    const auto size_noiseGLsizei = static_cast<GLsizei>(size_noise);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB16F, size_noiseGLsizei, size_noiseGLsizei,
                 0, GL_RGB, GL_FLOAT, ssaoNoise.data());
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
}


Matrix<float, Eigen::Dynamic, 3, Eigen::RowMajor> SSAO::create_noise (default_random_engine &generator,
                                                            uniform_real_distribution<float> &random) {
    const unsigned int num_noise = size_noise * size_noise;
    Matrix<float, Eigen::Dynamic, 3, Eigen::RowMajor> noise;
    noise.resize(num_noise, 3);

    AngleAxisf rotation(0.0f, Vector3f::UnitZ());
    for (unsigned int i = 0; i < num_noise; ++i) {        
        rotation.angle() = random(generator) * 2.0f * PI;
        noise.row(i) = rotation * Vector3f::UnitX();
    }

    return noise;
}


void SSAO::write_samples (default_random_engine &generator, uniform_real_distribution<float> &random) {
    Matrix<float, Eigen::Dynamic, 3, Eigen::RowMajor> samples;
    samples.resize(num_samples, 3);
    Matrix3f rotation;
    AngleAxisf rotationAroundZ(0.0f, Vector3f::UnitZ());
    AngleAxisf rotationAroundY(0.0f, Vector3f::UnitY());

    for (unsigned int i = 0; i < num_samples; ++i) {
        rotationAroundZ.angle() = random(generator) * 2.0f * PI;
        rotationAroundY.angle() = random(generator) * PI + PI;
        rotation = rotationAroundZ * rotationAroundY;
        samples.row(i) = rotation * Vector3f::UnitX();

        float scale = float(i) / num_samples;
        scale = linear_interpolation(0.1f, 1.0f, scale * scale);
        samples.row(i) *= random(generator) * scale;
    }

    shader.setUniform("samples", samples);
}


float SSAO::linear_interpolation (float a, float b, float delta) const {
    assert(delta >= 0.0f && delta <= 1.0f);
    return a + delta * (b - a);
}


/* ----------------------------------------------------------------------------------------*/

void SSAO::writeGaussianKernel() {
    const float sigma = (static_cast<float>(sizeBlurFilter) - 1.0f) / 6.0f;
    const unsigned int halfSizeFilter = sizeBlurFilter / 2;

    Eigen::VectorXf kernel;
    kernel.resize(halfSizeFilter + 1);

    const float a = -1.0f / (2.0f * sigma * sigma);
    const float b = 1.0f / (sqrt(2.0f * PI) * sigma);

    kernel[0] = b;
    float sumWeight = kernel[0];
    for (unsigned int x = 1; x <= halfSizeFilter; ++x) {
        float weight = static_cast<float>(x * x);
        weight = b * exp(a * weight);
        kernel[x] = weight;
        sumWeight += 2.0f * weight;
    }
    kernel /= sumWeight;

    shader_blur.setUniform("gaussianKernel", kernel);
}

void SSAO::setDepthBuffer(GLuint theDepthBuffer) {
    depthBuffer = theDepthBuffer;
}


void SSAO::setNormalBuffer(unsigned int theAttributeNormals) {
    attributeNormals = theAttributeNormals;
}


void SSAO::preLightingPass() {
    if (isActivated) {
        pass();
        pass_blur();

        Shader::deactivate();
        glBindFramebuffer(GL_FRAMEBUFFER, 0);
    }
}


void SSAO::pass () const {
    glBindFramebuffer(GL_FRAMEBUFFER, FBO);

    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, depthBuffer);

    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D, attributeNormals);

    glActiveTexture(GL_TEXTURE2);
    glBindTexture(GL_TEXTURE_2D, noiseTexture);

    glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT);

    shader.activate();
    Utility::renderScreenQuad(screenQuad_VAO);
}


void SSAO::pass_blur () {
    shader_blur.activate();

    glBindFramebuffer(GL_FRAMEBUFFER, FBOBlur);

    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, ssaoTarget);

    unsigned int filterDirection = HORIZONTAL;
    shader_blur.setUniform("filterDirection", filterDirection);
    Utility::renderScreenQuad(screenQuad_VAO);

    glBindFramebuffer(GL_FRAMEBUFFER, FBO);

    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, ssaoBlurTarget);

    filterDirection = VERTICAL;
    shader_blur.setUniform("filterDirection", filterDirection);
    Utility::renderScreenQuad(screenQuad_VAO);
}


void SSAO::update_projection (const Matrix4f &projection) {
    if (isActivated) {
        shader.activate();
        shader.setUniform("projection", projection);
        Shader::deactivate();
    }
}


/* ------------------ CallBacks -------------------------------*/

void SSAO::callback_frameBufferSize(unsigned int width, unsigned int height) const {
    if (isActivated) {
        glBindTexture(GL_TEXTURE_2D, ssaoTarget);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RED, width, height, 0, GL_RED, GL_FLOAT, nullptr);

        glBindTexture(GL_TEXTURE_2D, ssaoBlurTarget);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RED, width, height, 0, GL_RED, GL_FLOAT, nullptr);
        glBindTexture(GL_TEXTURE_2D, 0);
    }
}


void SSAO::user_interface(const Eigen::Matrix4f &projection) {
    if (ImGui::CollapsingHeader("SSAO")) {
        const bool previouslyActivated = isActivated;
        ImGui::Checkbox("Activate", &isActivated);

        if (isActivated) {

            if (!previouslyActivated) {
                unsigned int width, height;
                UserInterface::getWindowSize(width, height);
                callback_frameBufferSize(width, height);
                update_projection(projection);
            }

            auto num_samplesInt = reinterpret_cast<int *>(&num_samples);
            if (ImGui::DragInt("number of samples", num_samplesInt, 1, 5, 100)) {
                update_numSamples();
            }

            auto size_noiseInt = reinterpret_cast<int *>(&size_noise);
            if (ImGui::DragInt("size of noise", size_noiseInt, 1, 1, 100)) {
                update_sizeNoise();
            }

            if (ImGui::DragFloat("hemisphere radius", &hemisphere_radius, 0.01f, 0.0f, 10.0f)) {
                shader.activate();
                shader.setUniform("hemisphere_radius", hemisphere_radius);
                Shader::deactivate();
            }

            if (ImGui::DragFloat("tolerance depth", &tolerance_depth, 0.01f, 0.0f, 10.0f)) {
                shader.activate();
                shader.setUniform("tolerance_depth", tolerance_depth);
                Shader::deactivate();
            }

            if (ImGui::DragFloat("increment", &increment, 0.0001f, -1.0f, 1.0f, "%.4f")) {
                shader.activate();
                shader.setUniform("increment", increment);
                Shader::deactivate();
            }

            auto powerInt = reinterpret_cast<int *>(&power);
            if (ImGui::DragInt("power", powerInt, 1, 0, 10)) {
                shader_blur.activate();
                shader_blur.setUniform("power", power);
                Shader::deactivate();
            }

            auto size_blurFilterInt = reinterpret_cast<int *>(&sizeBlurFilter);
            if (ImGui::DragInt("size blur filter", size_blurFilterInt, 2, 3, 21)) {
                shader_blur.activate();
                shader_blur.setUniform("size_filter", sizeBlurFilter);
                writeGaussianKernel();
                Shader::deactivate();
            }
        }
    }
}


void SSAO::update_sizeNoise() {
    uniform_real_distribution<float> random(0.0f, 1.0f);
    default_random_engine generator;

    const auto ssaoNoise = create_noise(generator, random);
    glBindTexture(GL_TEXTURE_2D, noiseTexture);
    const auto size_noiseGLsizei = static_cast<GLsizei>(size_noise);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB16F, size_noiseGLsizei, size_noiseGLsizei,
                 0, GL_RGB, GL_FLOAT, ssaoNoise.data());
    glBindTexture(GL_TEXTURE_2D, 0);
}


void SSAO::update_numSamples() {
    uniform_real_distribution<float> random(0.0f, 1.0f);
    default_random_engine generator;

    shader.activate();
    shader.setUniform("num_samples", num_samples);
    write_samples(generator, random);
    Shader::deactivate();
}
