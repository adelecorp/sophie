#include "Phong.h"

#include <Eigen/Dense> // for inverse


constexpr int SSAO_UNIT = 2;

using namespace Eigen;

//---------------------------------------------------------------------------


Phong::Phong(SSAO *ssao) :
    Stylization(),
    geometryPass{"viewNormalPosition.vert.glsl", "viewNormalPosition.frag.glsl"},
    lightingPass{"simplePositionTexture.vert.glsl", "phong_lightingPass.frag.glsl"},
    lightPosition{15.0f, 0.0f, 0.0f, 1.0f},
    ssao{ssao}
{
    lightingPass.activate();
    lightingPass.setUniform("mapViewZ", 0);
    lightingPass.setUniform("gBuffer_inViewNormal", 1);
    lightingPass.setUniform("ssao", SSAO_UNIT);
    lightingPass.setUniform("lightColor", 1.0f, 1.0f, 1.0f);
    Shader::deactivate();
}


Phong::~Phong() {

}


void Phong::activateLightingShader() {
    lightingPass.activate();
    const bool ssaoIsActivated = ssao->getIsActivated();
    lightingPass.setUniform("activateSSAO", ssaoIsActivated);
}

void Phong::bindNeededTextures_lighting() const {
    if (ssao->getIsActivated()) {
        glActiveTexture(GL_TEXTURE0 + SSAO_UNIT);
        ssao->activate_blurBuffer();
    }
}


void Phong::updateViewAndModel (const Matrix4f &view, const Matrix4f &model) {
    const Matrix4f viewModel = view * model;
    const Matrix4f transposeInverseViewModel = viewModel.inverse().transpose();

    geometryPass.activate();
    geometryPass.setUniform("viewModel", viewModel);
    geometryPass.setUniform("transposeInverseViewModel", transposeInverseViewModel);

    // Update Lighting Pass Uniforms
    auto lightPosition_view = view * lightPosition;

    lightingPass.activate();
    lightingPass.setUniform("inViewLightPosition", lightPosition_view(0), lightPosition_view(1), lightPosition_view(2));
    Shader::deactivate();
}


void Phong::updateProjection(const Eigen::Matrix4f &projection) {
    geometryPass.activate();
    geometryPass.setUniform("projection", projection);

    lightingPass.activate();
    lightingPass.setUniform("projection", projection);

    Shader::deactivate();
}

