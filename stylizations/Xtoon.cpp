#include "Xtoon.h"

#include <imgui.h>
#include <Eigen/Dense> // for inverse


constexpr int XTOONMAP_UNIT = 3;
constexpr int SSAO_UNIT = 4;

using namespace Eigen;

//---------------------------------------------------------------------------

XToon::XToon(SSAO *ssao) :
    Stylization(),
    depthBased{false},
    geometryPass{"viewNormalPosition.vert.glsl", "viewNormalPosition.frag.glsl"},
    lightingPass{"simplePositionTexture.vert.glsl", "lightingPass_XToon.frag.glsl"},
    lightPosition{15.0f, 0.0f, 0.0f, 1.0f},
    effectMagnitude{2.0f},
    ssao{ssao}
{
    lightingPass.activate();
    lightingPass.setUniform("isDepthBased", depthBased);
    lightingPass.setUniform("effectMagnitude", effectMagnitude);
    lightingPass.setUniform("mapViewZ", 0);
    lightingPass.setUniform("gBuffer_inViewNormal", 1);
    lightingPass.setUniform("modulated_depth", 2);
    lightingPass.setUniform("xToonMap", XTOONMAP_UNIT);
    lightingPass.setUniform("ssao", SSAO_UNIT);
    Shader::deactivate();
}


XToon::~XToon() {

}


unsigned int XToon::neededGeometryAttributes() const {
    unsigned int num_neededAttributes = 0;

    if (depthBased) {
        num_neededAttributes = 1;
    }

    return num_neededAttributes;
}


void XToon::bindNeededTextures_lighting() const {
    glActiveTexture(GL_TEXTURE0 + XTOONMAP_UNIT);
    xToon_map.bind();

    if (ssao->getIsActivated()) {
        glActiveTexture(GL_TEXTURE0 + SSAO_UNIT);
        ssao->activate_blurBuffer();
    }
}


void XToon::activateGeometryShader() const {
    geometryPass.activate();
}


void XToon::activateLightingShader () {
    lightingPass.activate();
    const bool ssaoIsActivated = ssao->getIsActivated();
    lightingPass.setUniform("activateSSAO", ssaoIsActivated);
}


void XToon::updateViewAndModel(const Matrix4f &view, const Matrix4f &model) {
    const Matrix4f viewModel = view * model;
    const Matrix4f transposeInverseViewModel = viewModel.inverse().transpose();

    geometryPass.activate();
    geometryPass.setUniform("viewModel", viewModel);
    geometryPass.setUniform("transposeInverseViewModel", transposeInverseViewModel);

    // Update Lighting Pass Uniforms
    auto lightPosition_view = view * lightPosition;
    lightingPass.activate();
    lightingPass.setUniform("inViewLightPosition", lightPosition_view(0),
                            lightPosition_view(1), lightPosition_view(2));
    Shader::deactivate();
}


void XToon::updateProjection(const Matrix4f &projection) {
    geometryPass.activate();
    geometryPass.setUniform("projection", projection);

    lightingPass.activate();
    lightingPass.setUniform("projection", projection);

    Shader::deactivate();
}


void XToon::changeMode() {
    lightingPass.activate();
    lightingPass.setUniform("isDepthBased", depthBased);
    Shader::deactivate();
}


void XToon::user_interface() {
    if (ImGui::RadioButton("Orientation Based", !depthBased)) {
        depthBased = false;
        changeMode();
    }

    if (ImGui::RadioButton("Depth Based", depthBased)) {
        depthBased = true;
        changeMode();
    }

    if (!depthBased && ImGui::DragFloat("effect magnitude", &effectMagnitude, 1.0f, 0.0f, 10.0f)) {
        lightingPass.activate();
        lightingPass.setUniform("effectMagnitude", effectMagnitude);
        Shader::deactivate();
    }
}
