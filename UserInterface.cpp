#include "UserInterface.h"

#include "Dependencies/imgui-1.89.3/backends/imgui_impl_opengl3.h"
#include "Dependencies/imgui-1.89.3/backends/imgui_impl_glfw.h"



namespace { // anonymous namespace

void getWindowSizeInt (int &width, int &height) {
    const ImGuiIO& io = ImGui::GetIO();
    const auto window_size = io.DisplaySize;
    width = static_cast<int>(window_size.x);
    height = static_cast<int>(window_size.y);
}

} // anonymous namespace

/*----------------- Initialize -------------------------------------------*/

Camera *UserInterface::camera = nullptr;
Renderer *UserInterface::renderer = nullptr;

UserInterface::UserInterface(Window &window, unsigned int width, unsigned int height):
    window{window},
    accumulatedTime{0.0f},
    numDoneFrames{0},
    framesPerSecond{0.0f},
    offsetArraySavedFrameRates{0},
    accumulatedTimeForPlot{0.0f},
    numDoneFramesForPLot{0}
{
    for (unsigned int i = 0; i < NUM_SAVED_FRAMERATES; ++i) {
        savedFrameRates[i] = 0.0f;
    }

    UserInterface::renderer = new Renderer{width, height};
    UserInterface::camera = renderer->getCamera();

    ImGui::CreateContext();

    auto *glfwWindow = window.get_glfwWindow();
    set_callbacks(glfwWindow);

    // Setup Platform/Renderer bindings
    ImGui_ImplGlfw_InitForOpenGL(glfwWindow, true);
    ImGui_ImplOpenGL3_Init();

    update_size(width, height);
}


UserInterface::~UserInterface() {
    ImGui_ImplOpenGL3_Shutdown();
    ImGui_ImplGlfw_Shutdown();
    ImGui::DestroyContext();

    delete renderer;
}


void UserInterface::renderLoop() {
    const auto glfwWindow = window.get_glfwWindow();
    const ImGuiIO& io = ImGui::GetIO();

    while (!glfwWindowShouldClose(glfwWindow)) {
        renderer->render(); // Render a new frame
        window.update_deltaTime();
        updateFrameRate(io.DeltaTime);
        print();

        glfwSwapBuffers(glfwWindow);
        glfwPollEvents();
    }
}


void UserInterface::updateFrameRate(float timeSinceLastFrame) {
    accumulatedTimeForPlot += timeSinceLastFrame;
    ++numDoneFramesForPLot;

    if (accumulatedTimeForPlot >= 0.25f) {
        savedFrameRates[offsetArraySavedFrameRates] = numDoneFramesForPLot / accumulatedTimeForPlot;
        offsetArraySavedFrameRates = (offsetArraySavedFrameRates + 1) % NUM_SAVED_FRAMERATES;

        accumulatedTime += accumulatedTimeForPlot;
        numDoneFrames += numDoneFramesForPLot;

        if (accumulatedTime >= 2.0f) {
            framesPerSecond = numDoneFrames / accumulatedTime;
            accumulatedTime = 0.0f;
            numDoneFrames = 0;
        }

        accumulatedTimeForPlot = 0.0f;
        numDoneFramesForPLot = 0;
    }
}


void UserInterface::getWindowSize (unsigned int &width, unsigned int &height) {
    const ImGuiIO& io = ImGui::GetIO();
    const auto window_size = io.DisplaySize;
    width = static_cast<unsigned int>(window_size.x);
    height = static_cast<unsigned int>(window_size.y);
}


void UserInterface::set_callbacks (GLFWwindow *window) {
    glfwSetFramebufferSizeCallback(window, callback_frameBufferSize);
    glfwSetMouseButtonCallback(window, callback_mouse_pressedReleased);
    glfwSetCursorPosCallback(window, callback_mouseMovement);
    glfwSetScrollCallback(window, callback_scroll);
    glfwSetKeyCallback(window, callback_key);
}


/*----------------- CallBacks -------------------------------------------*/

void UserInterface::callback_frameBufferSize(GLFWwindow *, int new_width, int new_height) {
    const auto width = static_cast<unsigned int>(new_width);
    const auto height = static_cast<unsigned int>(new_height);

    update_size(width, height);
    renderer->callbackWindowSize(width, height);

    const int widthInt = static_cast<int>(width);
    const int heightInt = static_cast<int>(height);
    glViewport(0, 0, widthInt, heightInt);
}


void UserInterface::callback_mouse_pressedReleased (GLFWwindow *window, int button, int action, int) {
    const ImGuiIO &io = ImGui::GetIO();

    if (!io.WantCaptureMouse && button == GLFW_MOUSE_BUTTON_LEFT) {
        if (action == GLFW_PRESS) {
            int width, height;
            getWindowSizeInt(width, height);
            camera->process_leftMouse_pressed(window, width, height);
        } else if (action == GLFW_RELEASE) {
            camera->process_leftMouse_released(window);
        }
    }
}


void UserInterface::callback_mouseMovement(GLFWwindow *, double position_x, double position_y) {
    const ImGuiIO &io = ImGui::GetIO();

    if (!io.WantCaptureMouse) {
        int width, height;
        getWindowSizeInt(width, height);
        camera->process_mouseMovement(float(position_x), float(position_y), width, height);
    }
}


void UserInterface::callback_scroll (GLFWwindow *, double , double offset_y) {
    const ImGuiIO &io = ImGui::GetIO();

    if (!io.WantCaptureMouse) {
        camera->processMouseScroll(offset_y);
        renderer->updateProjection();
    }
}


void UserInterface::callback_key(GLFWwindow *window, int key, int , int action, int) {
    const ImGuiIO &io = ImGui::GetIO();

    if (!io.WantCaptureKeyboard) {
        if (action == GLFW_PRESS) {
            switch (key) {
            case GLFW_KEY_ESCAPE:
                glfwSetWindowShouldClose(window, true);
            break;
            case GLFW_KEY_SPACE: {
                const ImGuiIO& io = ImGui::GetIO();
                camera->process_keyboard(Camera::BREAK, io.DeltaTime);
            } break;
            default:
                callback_arrowKeys(key);
            break;
            }
        } else if (action == GLFW_REPEAT) {
            callback_arrowKeys(key);
        }
    }
}


void UserInterface::callback_arrowKeys(int key) {
    const ImGuiIO& io = ImGui::GetIO();

    switch (key) {
    case GLFW_KEY_UP:
        camera->process_keyboard(Camera::FORWARD, io.DeltaTime);
    break;
    case GLFW_KEY_DOWN:
        camera->process_keyboard(Camera::BACKWARD, io.DeltaTime);
    break;
    case GLFW_KEY_LEFT:
        camera->process_keyboard(Camera::LEFT, io.DeltaTime);
    break;
    case GLFW_KEY_RIGHT:
        camera->process_keyboard(Camera::RIGHT, io.DeltaTime);
    break;
    }
}

/*-----------------------------------------------------------------------*/


void UserInterface::update_size (unsigned int width, unsigned int height) {
    ImGuiIO& io = ImGui::GetIO();
    const float widthFloat = static_cast<int>(width);
    const float heightFloat = static_cast<int>(height);
    io.DisplaySize = ImVec2(widthFloat, heightFloat);
}


void UserInterface::print() const {
    ImGui_ImplOpenGL3_NewFrame();
    ImGui_ImplGlfw_NewFrame();
    ImGui::NewFrame();

    ImGui::Begin("Sophie", nullptr , ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_AlwaysAutoResize);

    // Frame Rate
    const double framesPerSecondDouble = static_cast<double>(framesPerSecond);
    ImGui::Text("Frame Rate : %.3f", framesPerSecondDouble);
    const int offsetArraySavedFrameRatesInt = static_cast<int>(offsetArraySavedFrameRates);
    ImGui::PlotLines("", savedFrameRates, NUM_SAVED_FRAMERATES, offsetArraySavedFrameRatesInt,
                     nullptr, 0.0f, 70.0f);
    ImGui::Separator();

    renderer->user_interface();

    ImGui::End();

    ImGui::Render();
    ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());
}
