import numpy as np
import matplotlib.pyplot as plot
import os
import argparse



def build_areas(max_edge_length):
	image_size = max_edge_length * 5
	areas = np.zeros((image_size, image_size, 2), dtype=np.float32)

	# x : distance to the extremity on the top/left
	# y : distance to the extremity on the bottom/right

	# [:, :, 0] : weight of the pixel on the bottom/right
	# [:, :, 1] : weight of the pixel on the top/left

	#------------------
	# Pattern
	#------------------
	#         |
	#  _______|  
	#-------------------
	subtexture = compute_LPattern(max_edge_length)
	areas[:max_edge_length, max_edge_length:2 * max_edge_length, 1] = subtexture

	#------------------
	# Pattern
	#------------------
	#  |       
	#  |_______  
	#-------------------
	# This is the same thing, except the roles of distanceLeft and distanceRight are inversed
	areas[max_edge_length:2 * max_edge_length, :max_edge_length, 1] = subtexture.T


	#-------------------------------
	# Patterns
	#-------------------------------
	#  _______        _______   
	#         |      |       
	#         |  and |
	#--------------------------------

	# This is the same as for these patterns :
	#         |      |       
	#  _______| and  |_______

	# except the areas coming from the triangle are on the pixels on the bottom/right
	# So the weights are on the first channel 
	areas[:max_edge_length, 3 * max_edge_length:4 * max_edge_length, 0] = subtexture
	# This is the same thing, except the roles of distanceLeft and distanceRight are inversed
	areas[3 * max_edge_length:4 * max_edge_length, :max_edge_length, 0] = subtexture.T

	#-------------------------------
	# Patterns
	#-------------------------------
	#         |      |
	#  _______|      |_______   
	#         |      |       
	#         |  and |
	#--------------------------------

	# We have both these patterns :
	#  _______        _______   
	#         |      |       
	#         |      |

	# and these patterns :
	#         |      |       
	#  _______|      |_______
	#
	# at the same time
	# So the weights are on both channels
	areas[:max_edge_length, 4 * max_edge_length:5 * max_edge_length, :] = np.dstack((subtexture, subtexture))
	# This is the same thing, except the roles of distanceLeft and distanceRight are inversed
	areas[4 * max_edge_length:5 * max_edge_length, :max_edge_length, :] = np.dstack((subtexture.T, subtexture.T))


	#--------------
	# Pattern
	#--------------
	#  |       |       
	#  |_______|
	#--------------

	subtextureHalfTriangle = compute_halfLPattern(max_edge_length)

	# As the triangles are symmetrical, the areas are the same when distanceLeft < distanceRight
	# The roles of distanceLeft and distanceRight are inversed
	areasUPattern = subtextureHalfTriangle + subtextureHalfTriangle.T
	areasRange = slice(max_edge_length, 2 * max_edge_length)
	areas[areasRange, areasRange, 1] = areasUPattern

	#--------------
	# Pattern
	#--------------
	#   _______
	#  |       |       
	#  |       |
	#--------------

	# This is the same as for this pattern :
	#  |       |       
	#  |_______|

	# except the areas coming from the triangles are on the pixels on the bottom/right
	# So the weights are on the first channel
	areasRange = slice(3 * max_edge_length, 4 * max_edge_length)
	areas[areasRange, areasRange, 0] = areasUPattern

	#--------------
	# Pattern
	#--------------
	#  |       |
	#  |_______|
	#  |       |       
	#  |       |
	#--------------

	# We have both this pattern :
	#  |       |       
	#  |_______|

	# and this pattern :
	#   _______
	#  |       |       
	#  |       |
	#
	# at the same time
	# So the weights are on both channels
	areasRange = slice(4 * max_edge_length, 5 * max_edge_length)
	areas[areasRange, areasRange, :] = np.dstack((areasUPattern, areasUPattern))

	#--------------
	# Pattern
	#--------------
	#  |       
	#  |_______
	#          |       
	#          |
	#--------------
	areasZPattern = np.dstack((subtextureHalfTriangle, subtextureHalfTriangle.T))
	areas[max_edge_length:2 * max_edge_length, 3 * max_edge_length:4 * max_edge_length, :] = areasZPattern

	#--------------
	# Pattern
	#--------------
	#          |
	#   _______|
	#  |              
	#  |       
	#--------------
	# This is the same thing, except the roles of distanceLeft and distanceRight are inversed
	areasZPattern = np.dstack((subtextureHalfTriangle.T, subtextureHalfTriangle))
	areas[3 * max_edge_length:4 * max_edge_length, max_edge_length:2 * max_edge_length, :] = areasZPattern


	#--------------
	# Pattern
	#--------------
	#  |       |
	#  |_______|
	#          |       
	#          |
	#--------------

	# This is the combination of this pattern :
	#  |       |       
	#  |_______|

	# and this triangle when y < x :
	#      ___      
	#         |
	#         | 

	areasPattern = np.dstack((subtextureHalfTriangle, areasUPattern))
	areas[max_edge_length:2 * max_edge_length, 4 * max_edge_length:5 * max_edge_length, :] = areasPattern

	#--------------
	# Pattern
	#--------------
	#  |       |
	#  |_______|
	#  |              
	#  |       
	#--------------
	# This is the same thing, except the roles of distanceLeft and distanceRight are inversed
	areasPattern = np.dstack((subtextureHalfTriangle.T, areasUPattern.T))
	areas[4 * max_edge_length:5 * max_edge_length, max_edge_length:2 * max_edge_length, :] = areasPattern


	#--------------
	# Pattern
	#--------------
	#          |
	#   _______|
	#  |       |       
	#  |       |
	#--------------

	# This is the combination of this triangle when y < x :
	#             
	#         |
	#      ___| 

	# And this pattern :
	#   _______
	#  |       |       
	#  |       |

	areasPattern = np.dstack((areasUPattern, subtextureHalfTriangle))
	areas[3 * max_edge_length:4 * max_edge_length, 4 * max_edge_length:5 * max_edge_length, :] = areasPattern

	#--------------
	# Pattern
	#--------------
	#  |       
	#  |_______
	#  |       |       
	#  |       |
	#--------------
	# This is the same thing, except the roles of distanceLeft and distanceRight are inversed
	areasPattern = np.dstack((areasUPattern.T, subtextureHalfTriangle.T))
	areas[4 * max_edge_length:5 * max_edge_length, 3 * max_edge_length:4 * max_edge_length, :] = areasPattern
	return areas


def compute_LPattern(max_edge_length):
	subtexture = np.empty((max_edge_length, max_edge_length))

	l = 1.0
	# L : The length of the longest edge of the big triangle
	L = np.arange(1, max_edge_length + 1)
	# From the Thales theorem :
	# l / L = triangleEdge / 0.5
	triangleEdge = (l * 0.5) / L
	# Area of a right triangle : (a * b) * 0.5
	subtexture[0, :] = (triangleEdge * l) * 0.5

	for distanceLeft in range(1, max_edge_length):
		for distanceRight in range(max_edge_length):
			# +1.0 for the current pixel (not taken into account in any of the distances)
			L = distanceLeft + distanceRight + 1.0

			l = distanceLeft
			# From the Thales theorem :
			# l / L = firstTrapezoidEdge / 0.5
			firstTrapezoidEdge = (l * 0.5) / L

			l = l + 1.0
			# From the Thales theorem :
			# l / L = secondTrapezoidEdge / 0.5
			secondTrapezoidEdge = (l * 0.5) / L

			# Area of a trapezoid : 0.5 * (lengthBase1 + lengthBase2) * height
			# Here, height is always equal to 1.0 (the length of each pixel edge)
			trapezoid_area = 0.5 * (firstTrapezoidEdge + secondTrapezoidEdge)
			subtexture[distanceLeft][distanceRight] = trapezoid_area

	return subtexture


def compute_halfLPattern(max_edge_length):
	subtexture = np.zeros((max_edge_length, max_edge_length))

	# Diagonal where distanceLeft = distanceRight
	# The area is a triangle which will be later multiplied by 2 (symmetric triangles)
	distanceRight = np.arange(0, max_edge_length)
	# L : The length of the longest edge of the big triangle
	L = distanceRight + 0.5
	l = 0.5
	# From the Thales theorem :
	# triangleEdge / 0.5 = l / L
	triangleEdge = (l * 0.5) / L
	# Area of a right triangle : (a * b) * 0.5
	triangle_area = (triangleEdge * l) * 0.5
	np.fill_diagonal(subtexture, triangle_area)

	# For all distanceRight < distanceLeft
	for distanceLeft in range(max_edge_length):
		for distanceRight in range(distanceLeft):
			# L : The length of the longest edge of the big triangle
			L = 0.5 * (distanceLeft + distanceRight + 1.0)

			l = L - (distanceRight + 1.0)
			# From the Thales theorem :
			# firstTrapezoidEdge / 0.5 = l / L
			firstTrapezoidEdge = (l * 0.5) / L

			l = l + 1
			# From the Thales theorem :
			# secondTrapezoidEdge / 0.5 = l / L
			secondTrapezoidEdge = (l * 0.5) / L

			# Area of a trapezoid : 0.5 * (lengthBase1 + lengthBase2) * height
			# Here, height is always equal to 1.0 (the length of each pixel edge)
			trapezoid_area = 0.5 * (firstTrapezoidEdge + secondTrapezoidEdge)
			subtexture[distanceLeft][distanceRight] = trapezoid_area

	return subtexture


def display_areas(areas):
	image_size = areas.shape[0]
	image = np.dstack((areas, np.zeros((image_size, image_size))))
	plot.imshow(image, interpolation="nearest", vmin=0.0, vmax=1.0)
	max_edge_length = areas.shape[0] / 5
	axis_ticks = np.arange(0, image_size, max_edge_length)
	plot.xticks(axis_ticks)
	plot.yticks(axis_ticks)
	plot.show()


def read_areaFile(filename):
	areas = np.fromfile(filename, dtype=np.float32)
	image_size = int(np.sqrt(areas.shape[0] / 2))
	areas = np.reshape(areas, (image_size, image_size, 2))
	return areas


def write_areasIntoFile(areas, directory):
	areasIntoBytes = bytes(areas)
	max_edge_length = int(areas.shape[0] / 5)
	filename = "areas_" + str(max_edge_length) + ".bin"
	path = os.path.join(directory, filename)
	with open(path, "wb") as file:
		file.write(areasIntoBytes)


def generate_areaMap(arguments):
    areas = build_areas(arguments.max_edge)
    write_areasIntoFile(areas, arguments.directory)


def display_areaMap(arguments):
	areas = read_areaFile(arguments.areas_path)
	display_areas(areas)



parser = argparse.ArgumentParser()
subparsers = parser.add_subparsers(required=True)
# Generate Command
parser_generate = subparsers.add_parser("generate",
					help="Generate an area map and write it into a binary file.")
parser_generate.add_argument("--max_edge", type=int, default=9, choices=range(1, -1),
                    help="The maximum distance the pixel can have to an edge extremity.")
parser_generate.add_argument("--directory", type=str, default=".",
                    help="The directory were the area file will be written into.")
parser_generate.set_defaults(func=generate_areaMap)
# Display Command
parser_display = subparsers.add_parser("display", help="Display the given area map.")
parser_display.add_argument("areas_path", type=str, 
					help="The file path of the area map to display.")
parser_display.set_defaults(func=display_areaMap)

arguments = parser.parse_args()
arguments.func(arguments)

