//#version 450 core

//#include include/utility.glsl


in vec2 texture_coords;

uniform bool activateSSAO;
uniform vec3 inViewLightPosition;
uniform vec3 lightColor;

uniform mat4 projection;

uniform sampler2D mapViewZ;
uniform sampler2D gBuffer_inViewNormal;
uniform sampler2D ssao;

out vec4 color;


void main() {
    vec3 inViewNormal = texture(gBuffer_inViewNormal, texture_coords).xyz;
    if (inViewNormal == vec3(0.0)) {
        discard;
    } else {
        const vec3 inViewPosition = constructViewPosition(mapViewZ, projection, texture_coords);
        inViewNormal = normalize(inViewNormal);
        const vec3 diffuse_material = vec3(0.0f, 1.0f, 1.0f);

        // ambient
        vec3 ambient = 0.3f * diffuse_material;
        // diffuse
        const vec3 lightDir = normalize(inViewLightPosition - inViewPosition);
        const float amountDiffuse = max(dot(inViewNormal, lightDir), 0.0) * 0.5f;
        const vec3 diffuse =  amountDiffuse * diffuse_material * lightColor;
        // specular
        const vec3 viewDir  = normalize(-inViewPosition);
        const vec3 halfwayDir = normalize(lightDir + viewDir);
        float amountSpecular = max(dot(inViewNormal, halfwayDir), 0.0);
        amountSpecular = pow(amountSpecular, 800.0) * 0.5f;
        const vec3 specular = lightColor * amountSpecular;

        if (activateSSAO) {
            const float occlusion = texture(ssao, texture_coords).x;
            ambient *= occlusion;
        }

        const vec3 result = ambient + diffuse + specular;
        color = vec4(result, 1.0f);

        //color = vec4(inViewNormal * 0.5 + 0.5, 1.0f);
        //const float occlusion = texture(ssao, texture_coords).x;
        //color = vec4(occlusion, occlusion, occlusion, 1.0f);
    }
}
