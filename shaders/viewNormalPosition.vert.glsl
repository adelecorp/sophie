//#version 450 core

layout (location = 0) in vec3 position;
layout (location = 1) in vec3 normal;

uniform mat4 viewModel;
uniform mat4 transposeInverseViewModel;
uniform mat4 projection;

out vec3 inViewNormal;
out float inViewZ;

void main() {
    const vec4 viewPosition = viewModel * vec4(position, 1.0f);
    inViewZ = viewPosition.z;
    gl_Position = projection * viewPosition;
    inViewNormal = mat3(transposeInverseViewModel) * normal;
}
