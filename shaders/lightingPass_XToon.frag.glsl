//#version 450 core

//#include include/utility.glsl

in vec2 texture_coords;

uniform bool activateSSAO;
uniform bool isDepthBased;
uniform vec3 inViewLightPosition;

uniform mat4 projection;

uniform sampler2D mapViewZ;
uniform sampler2D gBuffer_inViewNormal;
uniform sampler2D xToonMap;
uniform sampler2D ssao;

// Orientation Based
uniform float effectMagnitude;

out vec4 color;


void main() {
    vec3 inViewNormal = texture(gBuffer_inViewNormal, texture_coords).xyz;
    if (inViewNormal == vec3(0.0)) {
        discard;
    } else {
        const vec3 inViewPosition = constructViewPosition(mapViewZ, projection, texture_coords);
        inViewNormal = normalize(inViewNormal);

        // Light Exposure
        const vec3 lightDir = normalize(inViewLightPosition - inViewPosition);
        const vec3 viewDir  = normalize(-inViewPosition);
        float lightExposure = 1.0f - max(dot(inViewNormal, lightDir), 0.0f);

        if (activateSSAO) {
            const float occlusion = texture(ssao, texture_coords).x;
            lightExposure = mix(1.0f, lightExposure, occlusion);
        }

        float ySamplingPosition;
        if (isDepthBased) {
            const float z = -texture(mapViewZ, texture_coords).x;

            // Future Uniform Values
            const float z_min = 2.0f;
            const float scale = 2.0f;

            const float z_max = scale * z_min;
            ySamplingPosition = 1.0f - log(z / z_min) / log(z_max / z_min);
        } else {
            // Orientation Based
            ySamplingPosition = pow(abs(dot(inViewNormal, viewDir)), effectMagnitude);            
        }

        const vec2 samplingPosition = vec2(lightExposure, ySamplingPosition);
        vec3 colorXToon = texture(xToonMap, samplingPosition).xyz;

        color = vec4(colorXToon, 1.0f);
    }
}
