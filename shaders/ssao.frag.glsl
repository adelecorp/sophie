//#version 460 core

//#include include/utility.glsl

#define MAX_SAMPLES 128

uniform sampler2D depthBuffer;
uniform sampler2D mapViewNormals;
uniform sampler2D buffer_noise;

uniform vec3 samples[MAX_SAMPLES];
uniform mat4 projection;

// Tunable values
uniform uint num_samples;
uniform float hemisphere_radius;
uniform float tolerance_depth;
uniform float increment;


in vec2 texture_coords;

out float outOcclusion;


void main() {
    vec3 normal = texture(mapViewNormals, texture_coords).xyz;
    if (normal == vec3(0.0)) {
        discard;
    } else {
        vec2 noiseScale = textureSize(depthBuffer, 0);
        noiseScale /= textureSize(buffer_noise, 0);

        const vec3 position = constructViewPosition(depthBuffer, projection, texture_coords);
        normal = normalize(normal);
        const vec3 noise = normalize(texture(buffer_noise, texture_coords * noiseScale).xyz);

        const vec3 tangent = normalize(noise - normal * dot(noise, normal));
        const vec3 bitangent = cross(normal, tangent);
        // TBN
        mat4 transformationSample = mat4(vec4(tangent, 0.0f), vec4(bitangent, 0.0f), vec4(normal, 0.0f), vec4(0.0f));
        // Add position and multiply by hemisphere_radius
        transformationSample[0] *= hemisphere_radius;
        transformationSample[1] *= hemisphere_radius;
        transformationSample[2] *= hemisphere_radius;
        transformationSample[3] = vec4(position, 1.0f);
        // Project x, y, and w into NDC space, keep z in view space
        mat4 projectionWithoutZ = projection;
        projectionWithoutZ[0][2] = 0.0f;
        projectionWithoutZ[1][2] = 0.0f;
        projectionWithoutZ[2][2] = 1.0f;
        projectionWithoutZ[3][2] = 0.0f;
        transformationSample = projectionWithoutZ * transformationSample;

        float occlusion = 0.0f;
        for (uint i = 0; i < num_samples; ++i) {
            const vec4 transformedSample = transformationSample * vec4(samples[i], 1.0f);
            const float sampleViewZ = transformedSample.z;

            vec2 sampleInClipSpace = transformedSample.xy /transformedSample.w;
            sampleInClipSpace = sampleInClipSpace * 0.5f + 0.5f;

            const float sample_closestDepth = texture(depthBuffer, sampleInClipSpace).x;

            // Approximation of step function
            const float k = 2000.0f; // The higher it is, the closest it is to the step function
            const float contribution = 0.5f + 0.5f * tanh(k * (sample_closestDepth - (sampleViewZ + increment)));
            //const float contribution = step(sampleViewZ, sample_closestDepth - increment);

            float rangeCheck = tolerance_depth / abs(position.z - sample_closestDepth);
            rangeCheck = smoothstep(0.0f, 1.0f, rangeCheck);

            occlusion += contribution * rangeCheck;
        }

        outOcclusion = 1.0f - (occlusion / num_samples);
    }
}
