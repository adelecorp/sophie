//#version 460 core

const vec2 triangleVertices[3] = vec2[3](vec2(-1.0f, -1.0f), vec2(3.0f, -1.0f), vec2(-1.0f, 3.0f));

out vec2 texture_coords;

void main() {
    gl_Position = vec4(triangleVertices[gl_VertexID], 0.0f, 1.0f);
    texture_coords = 0.5f * gl_Position.xy + 0.5f;
}
