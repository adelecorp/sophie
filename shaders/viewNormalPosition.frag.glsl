//#version 450 core

layout (location = 0) out float viewZ;
layout (location = 1) out vec3 viewNormal;

in float inViewZ;
in vec3 inViewNormal;

void main() {
    viewZ = inViewZ;
    viewNormal = inViewNormal;
}
