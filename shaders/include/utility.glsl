

vec3 constructViewPosition(sampler2D mapViewZ, mat4 projection, vec2 uv) {
    const float viewZ = texture(mapViewZ, uv).x;
    // Normalized Device Coordinates unit cube [-1,1]
    const vec2 pointNDC = vec2(uv.x, uv.y) * 2.0f - 1.0f;

    vec3 pointView;
    pointView.x = -viewZ * pointNDC.x / projection[0][0];
    pointView.y = -viewZ * pointNDC.y / projection[1][1];
    pointView.z = viewZ;

    return pointView;
}
