//#version 450 core

in vec2 frag_uv;
in vec4 frag_color;

uniform sampler2D theTexture;

layout (location = 0) out vec4 color;

void main() {
    color = frag_color * texture(theTexture, frag_uv.st);
}