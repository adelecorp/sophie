//#version 460 core

uniform sampler2D inputTexture;

in vec2 texture_coords;

out float luminance;

void main() {
	const vec3 color = texelFetch(inputTexture, ivec2(gl_FragCoord.xy), 0).rgb;
	luminance = 0.2126f * color.r + 0.7152f * color.g + 0.0722f * color.b;
}
