//#version 460 core

uniform sampler2D luminanceTexture;
uniform float threshold;

in vec2 texture_coords;

out vec2 edges;

void main() {	
    const vec2 texelSize = 1.0f / textureSize(luminanceTexture, 0);

    const float luminance = texture(luminanceTexture, texture_coords).r;

    const vec2 leftTextureCoords = vec2(texture_coords.x - texelSize.x, texture_coords.y);
    const float leftLuminance = texture(luminanceTexture, leftTextureCoords).r;

    const vec2 topTextureCoords = vec2(texture_coords.x, texture_coords.y + texelSize.y);
    const float topLuminance = texture(luminanceTexture, topTextureCoords).r;

    edges = abs(vec2(leftLuminance, topLuminance) - luminance.xx);
    edges = step(threshold.xx, edges);
}
