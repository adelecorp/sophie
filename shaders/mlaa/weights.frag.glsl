//#version 460 core

uniform sampler2D areaMap;
uniform sampler2D edgesTexture;

uniform uint maxEdgeLength;

in vec2 texture_coords;

out vec4 weights;


float searchHorizontalDistanceToExtremity(vec2 texelSize, float direction) {
    const uint maxSearchSteps = maxEdgeLength / 2;
    const float offset = direction * texelSize.x;
    vec2 currentTexcoord = vec2(texture_coords.x + 1.5f * offset, texture_coords.y);
    float end = 0.0f;
    uint i = 0;
    for (   ; i < maxSearchSteps; i++) {
        // Green channel for the edge at the top
        end = texture(edgesTexture, currentTexcoord).g;
        if (end < 0.9f) {
            break;
        }

        currentTexcoord.x += 2.0f * offset;
    }

    return min(round(2.0f * i + 2.0f * end), maxEdgeLength - 1.0f) * direction;
}


float searchVerticalDistanceToExtremity(vec2 texelSize, float direction) {
    const uint maxSearchSteps = maxEdgeLength / 2;
    const float offset = direction * texelSize.y;
    vec2 currentTexcoord = vec2(texture_coords.x, texture_coords.y + 1.5f * offset);
    float end = 0.0f;
    uint i = 0;
    for (   ;i < maxSearchSteps; i++) {
        // Red channel for the edge at the left
        end = texture(edgesTexture, currentTexcoord).r;
        if (end < 0.9f) {
            break;
        }

        currentTexcoord.y += 2.0f * offset;
    }

    return min(round(2.0f * i + 2.0f * end), maxEdgeLength - 1.0f) * direction;
}


vec2 computeAreas(vec2 distanceToExtremity, float extremity1, float extremity2) {
    // Rounding prevents bilinear access precision problems
    vec2 texCoord = maxEdgeLength * round(4.0f * vec2(extremity1 , extremity2));
    texCoord += distanceToExtremity;
    // r : area of the pixel on the bottom/right of the edge
    // g : area of the pixel on the top/left of the edge
    return texelFetch(areaMap, ivec2(texCoord), 0).rg;
}


void main() {
    weights = vec4(0.0f);

    const vec2 texelSize = 1.0f / textureSize(edgesTexture, 0);
    const vec2 edges = texelFetch(edgesTexture, ivec2(gl_FragCoord.xy), 0).rg;

    if (edges.g > 0.0f) { // Edge on the top
        const float leftDistance = searchHorizontalDistanceToExtremity(texelSize, -1.0f);
        const float rightDistance = searchHorizontalDistanceToExtremity(texelSize, 1.0f);

        // 0.25f to distinguish between two patterns
        const vec2 leftExtremityCoord = texture_coords + vec2(leftDistance, 0.25f) * texelSize;
        // +1.0f to search the edge on the left
        const vec2 rightExtremityCoord = texture_coords + vec2(rightDistance + 1.0f, 0.25f) * texelSize;

        const float extremityLeft = texture(edgesTexture, leftExtremityCoord).r; 
        const float extremityRight = texture(edgesTexture, rightExtremityCoord).r;

        const vec2 distanceToExtremity = abs(vec2(leftDistance, rightDistance));
        // r : area on the bottom of the edge
        // g : area on the top of the edge
        weights.rg = computeAreas(distanceToExtremity, extremityLeft, extremityRight);
    }

    if (edges.r > 0.0f) { // Edge on the left
        const float upDistance = searchVerticalDistanceToExtremity(texelSize, 1.0f);
        const float downDistance = searchVerticalDistanceToExtremity(texelSize, -1.0f);

        // -0.25f to distinguish between two patterns
        const vec2 upExtremityCoord = texture_coords + vec2(-0.25f, upDistance) * texelSize;
        // -1.0f to search the edge on top
        const vec2 downExtremityCoord = texture_coords + vec2(-0.25f, downDistance - 1.0f) * texelSize;

        const float extremityUp = texture(edgesTexture, upExtremityCoord).g; 
        const float extremityDown = texture(edgesTexture, downExtremityCoord).g;

        const vec2 distanceToExtremity = abs(vec2(upDistance, downDistance));
        // b : area on the right of the edge
        // a : area on the left of the edge
        weights.ba = computeAreas(distanceToExtremity, extremityUp, extremityDown);
    }
}
