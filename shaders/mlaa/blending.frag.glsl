//#version 460 core

uniform sampler2D aliasedTexture;
uniform sampler2D weightTexture;

in vec2 texture_coords;

out vec4 result;


void main() {
	const vec4 topLeft = texelFetch(weightTexture, ivec2(gl_FragCoord.xy), 0);

	ivec2 fetchCoords = ivec2(gl_FragCoord.x, gl_FragCoord.y - 1.0f);
	const float bottom = texelFetch(weightTexture, fetchCoords, 0).g;

	fetchCoords = ivec2(gl_FragCoord.x + 1.0f, gl_FragCoord.y);
	const float right = texelFetch(weightTexture, fetchCoords, 0).a;

	const vec4 areas = vec4(topLeft.r, bottom, topLeft.b, right);
	const float sum = dot(areas, vec4(1.0f));

	if (sum > 0.0f) {
		// There is anti-aliasing to do on at least one edge
		const vec2 texelSize = 1.0f / textureSize(aliasedTexture, 0);
		const vec4 areasOffset = areas * texelSize.yyxx;

		vec2 texCoords = vec2(texture_coords.x, texture_coords.y + areasOffset.r);
		vec4 color = areas.r * texture(aliasedTexture, texCoords);

		texCoords = vec2(texture_coords.x, texture_coords.y - areasOffset.g);
		color += areas.g * texture(aliasedTexture, texCoords);

		texCoords = vec2(texture_coords.x - areasOffset.b, texture_coords.y);
		color += areas.b * texture(aliasedTexture, texCoords);

		texCoords = vec2(texture_coords.x + areasOffset.a, texture_coords.y);
		color += areas.a * texture(aliasedTexture, texCoords);

		result = color / sum;
	} else {
		result = texelFetch(aliasedTexture, ivec2(gl_FragCoord.xy), 0);
	}
}
