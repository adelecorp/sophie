//#version 450 core

#define MAX_HALF_GAUSSIAN_FILTER_SIZE_PLUS_ONE 11


#define HORIZONTAL 0
#define VERTICAL 1
uniform uint filterDirection;

uniform float gaussianKernel[MAX_HALF_GAUSSIAN_FILTER_SIZE_PLUS_ONE];

uniform sampler2D textureToFilter;

// Tunable values
uniform uint power;
uniform uint size_filter;

in vec2 texture_coords;

out float outResult;


void main() {
    vec2 step = (filterDirection == HORIZONTAL) ? vec2(1.0f, 0.0f) : vec2(0.0f, 1.0f);
    const vec2 texel_size = 1.0f / textureSize(textureToFilter, 0);
    step *= texel_size;

    float weight = gaussianKernel[0];
    float result = weight * texture(textureToFilter, texture_coords).x;

    const int half_sizeFilter = int(size_filter) / 2;
    for (int i = 1; i <= half_sizeFilter; ++i) {
        const vec2 offset = float(i) * step;

        weight = gaussianKernel[i];
        result += weight * texture(textureToFilter, texture_coords + offset).x;
        result += weight * texture(textureToFilter, texture_coords - offset).x;
    }

    outResult = result;

    if (filterDirection == VERTICAL) {
        outResult = pow(outResult, power);
    }
}
