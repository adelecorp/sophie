#include "Window.h"

#include <imgui.h>



//------------------------------------------------------------------

Window::Window(unsigned int width, unsigned int height, const char *title) :
    lastFrame{0.0f}
{
    init_glfwWindow();
    createWindow(width, height, title);
    glfwMakeContextCurrent(window);
    init_glad();
}


Window::~Window() {
    glfwDestroyWindow(window);
    glfwTerminate();
}


void Window::init_glad () {
    if ( !gladLoadGLLoader( (GLADloadproc)glfwGetProcAddress ) ) {
        throw ErrorWindow{"Failed to initialize GLAD"};
    }
}


void Window::init_glfwWindow() {
    glfwInit();
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 6);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    // For Apple
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);

    glfwWindowHint(GLFW_VISIBLE, GL_FALSE);
}


void Window::createWindow (int width, int height, const char *title) {
    window = glfwCreateWindow(width, height, title, nullptr, nullptr);
    if (!window) {
        throw ErrorWindow{"Failed to create GLFW window"};
    }
}


void Window::update_deltaTime () {
    ImGuiIO& io = ImGui::GetIO();
    const auto currentFrame = float(glfwGetTime());

    io.DeltaTime = currentFrame - lastFrame;
    lastFrame = currentFrame;
}


Window::ErrorWindow::ErrorWindow(const std::string &message_error):
    std::runtime_error{message_error} {}
