#pragma once

#include <glad/glad.h>
#include <iostream>


class Shader {
private:
    GLuint ID;

public:
    Shader(const std::string &vertexPath, const std::string &fragmentPath);

    ~Shader();

    inline void activate() const {
        glUseProgram(ID);
    }

    inline static void deactivate() {
        glUseProgram(0);
    }

    /* Utility uniform fonctions */

    template<typename Type>
    void setUniform(const std::string &name, const Type &value);

    template<typename Type>
    void setUniform(const std::string &name, Type value1, Type value2, Type value3);

    //---------------------

    class ErrorShader : public std::runtime_error {
    public:
        explicit ErrorShader(const std::string &message_error);
    };

private:
    /* Methods */

    inline int getLocationUniform (const std::string &name) {
        return glGetUniformLocation(ID, name.c_str());
    }

};

