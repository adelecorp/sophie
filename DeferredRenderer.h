#pragma once

#include "Mesh.h"

#include "stylizations/Stylization.h"
#include "stylizations/Xtoon.h"
#include "stylizations/Phong.h"

#include "stylizations/SSAO.h"
#include "stylizations/MLAA.h"

#include <vector>


class DeferredRenderer {
public:
    DeferredRenderer(unsigned int width, unsigned int height);
    ~DeferredRenderer();

    void update_viewAndModel(const Eigen::Matrix4f &view, const Eigen::Matrix4f &model);
    void update_projection(const Eigen::Matrix4f &projection);

    void geometry_pass(const Mesh &mesh) const;
    void lightingPass() const;

    void user_interface(const Eigen::Matrix4f &projection);
    void callback_frameBufferSize(unsigned int width, unsigned int height);

private:
    /* -------------------*/
    /* Attributes */
    /* -------------------*/

    std::vector<unsigned int> attributeBuffers;
    unsigned int depthBuffer;
    GLuint geometricFramebuffer;

    GLuint aliasedFrameBuffer;
    GLuint aliasedResult;

    GLuint screenQuad_VAO;

    bool mode_wireframe;

    SSAO *ssao;
    MLAA *mlaa;

    Stylization *stylization;
    XToon *xToon;
    Phong *phong;

    /* -------------------*/
    /* Functions */
    /* -------------------*/

    void configureGeometricFrameBuffer(GLsizei width, GLsizei height);
    void configureViewDepthMap(GLsizei width, GLsizei height, unsigned int attachment) const;
    void configureNormalBuffer(GLsizei width, GLsizei height, unsigned int attachment) const;
    void create_depthBuffer(GLsizei width, GLsizei height);
};
