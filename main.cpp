#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include "Window.h"
#include "UserInterface.h"


//-------------------------------------------------------------------------------------------------------------

int main() {
    const unsigned int width = 700;
    const unsigned int height = 700;

    Window window{width, height, "Sophie"};
    auto glfwWindow = window.get_glfwWindow();

    UserInterface userInterface{window, width, height};

    glfwShowWindow(glfwWindow);
    userInterface.renderLoop();

    exit(0);
}
