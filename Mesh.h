#pragma once

#include <glad/glad.h>
#include <string>
#include <mutex>

#include <Eigen/Core>


class Mesh {
public:
    Mesh(const std::string &filePath);

    ~Mesh();

    void update();

    void getModelMatrix(Eigen::Matrix4f &modelMatrix) const;

    void draw() const;

    void user_interface();

    // level : subdivision level;
    //void subdivideWith_loop (unsigned int level);

    //---------------------

    class ErrorMesh : public std::runtime_error {
    public:
        ErrorMesh(const std::string &message_error);
    };

private:
    /* Definitions */

    using MatrixX3fR = Eigen::Matrix<GLfloat, Eigen::Dynamic, 3, Eigen::RowMajor>;
    using MatrixX3uiR = Eigen::Matrix<GLuint, Eigen::Dynamic, 3, Eigen::RowMajor>;

    /* Attributes */

    MatrixX3fR vertices, normals;
    MatrixX3uiR faces;
    unsigned int numFaces;

    GLuint VAO, VBO[2], EBO;

    std::string filePath;

    Eigen::Matrix4f modelMatrix;

    // Load new mesh
    bool newMeshLoading;
    std::atomic_bool newMeshToLoad;
    std::atomic<float> progressFraction;

    /* Functions */

    void loadMesh();
    bool read_file ();

    void updateVAO();
    void write_vbo (unsigned int index_attribute, const MatrixX3fR &attributes);

    void compute_normals();
    void computeNormals(float progressStep, std::mutex mutexes[], unsigned int rangeMutexes,
                        unsigned int rangeBegin, unsigned int rangeEnd);

    void initialize_vbo(unsigned int index_attribute, GLsizei stride);

    void computeModelMatrix();
};
