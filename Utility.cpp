#include "Utility.h"

#include <assert.h>
#include <stdexcept>


namespace Utility {
    void allocateBuffer(GLsizei width, GLsizei height, unsigned int numCoefficients,
                        GLint internalformat, GLenum type) {
        GLenum format = 0;

        switch (numCoefficients) {
        case 1:
            format = GL_RED;
            break;
        case 2:
            format = GL_RG;
            break;
        case 4:
            format = GL_RGBA;
            break;
        default:
            assert("allocateBuffer : Number of coefficients not supported.");
            break;
        }

        glTexImage2D(GL_TEXTURE_2D, 0, internalformat, width, height, 0, format, type, nullptr);
    }


    void configureFrameBuffer(GLuint &FBO, GLuint &target, GLsizei width, GLsizei height,
                              unsigned int numCoefficients, GLint internalformat,
                              GLint textureFilter, GLint textureWrap, GLenum type) {
        glGenFramebuffers(1, &FBO);
        glBindFramebuffer(GL_FRAMEBUFFER, FBO);

        glGenTextures(1, &target);
        glBindTexture(GL_TEXTURE_2D, target);
        allocateBuffer(width, height, numCoefficients, internalformat, type);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, textureFilter);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, textureFilter);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, textureWrap);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, textureWrap);
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, target, 0);
        glBindTexture(GL_TEXTURE_2D, 0);

        // finally check if frame Buffer is complete
        if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE) {
            throw std::runtime_error{"Frame Buffer not complete"};
        }

        glBindFramebuffer(GL_FRAMEBUFFER, 0);
    }


    void renderScreenQuad(GLuint screenQuad_VAO) {
        glBindVertexArray(screenQuad_VAO);
        glDrawArrays(GL_TRIANGLES, 0, 3);
        glBindVertexArray(0);
    }
}
