In order to launch Sophie, you can execute the following commands in a terminal :

mkdir build

cd build

cmake <path to Sophie Directory>

make

./Sophie

where <Sophie Directory> is the path of the directory called "sophie".

Requirements
--------------

C++20
glfw3 library, version 3.3
OpenGL 4.6

<p align="center">
	<img src="screenshots/bunnySSAO.png" width="350" alt="Rendu avec SSAO">
</p>

<p align="center">
	<img src="screenshots/bunnyXTOON.png" width="350" alt="Rendu avec XTOON">
</p>

<p align="center">
	<img src="screenshots/parametres.png" width="350" alt="Paramètres de l'application.png">
</p>

