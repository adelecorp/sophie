#pragma once

#include <glad/glad.h>
#include <map>
#include <vector>

#include <Eigen/Core>



class HalfEdgeStructure {
public:

    using MatrixX3uiR = Eigen::Matrix<GLuint, Eigen::Dynamic, 3, Eigen::RowMajor>;
    HalfEdgeStructure(const MatrixX3uiR &faces);

    std::vector<unsigned int> get_neighbors (long index_vertex) const;

    ~HalfEdgeStructure();

private:

    /* Definitions */
    struct HalfEdge;
    struct Vertex;

    // TODO : Replace by unordered map
    using MapHalfEdges = std::map< std::pair<unsigned int, unsigned int>, HalfEdge * >;

    /* Attributes */

    // TODO : array ?
    std::vector<Vertex> vertices;
    HalfEdge *halfEdges;
};

