#include "DeferredRenderer.h"

#include <imgui.h>

#include "Utility.h"
#include "UserInterface.h"


enum Attribute {
    DEPTH,
    NORMAL
};


using namespace std;
using namespace Eigen;

//---------------------------------------------------------------------------


DeferredRenderer::DeferredRenderer(unsigned int width, unsigned int height) :
    mode_wireframe{false}
{
    glGenVertexArrays(1, &screenQuad_VAO);
    Utility::configureFrameBuffer(aliasedFrameBuffer, aliasedResult,
                                  width, height, 4, GL_RGBA, GL_LINEAR);

    ssao = new SSAO{width, height, screenQuad_VAO};
    mlaa = new MLAA{width, height, screenQuad_VAO, aliasedResult};

    phong = new Phong{ssao};
    xToon = new XToon{ssao};

    stylization = xToon;

    configureGeometricFrameBuffer(width, height);

    ssao->setDepthBuffer(attributeBuffers[DEPTH]);
    ssao->setNormalBuffer(attributeBuffers[NORMAL]);
}


DeferredRenderer::~DeferredRenderer() {
    glDeleteVertexArrays(1, &screenQuad_VAO);
    glDeleteFramebuffers(1, &geometricFramebuffer);
    const auto numAttributeBuffers = static_cast<GLsizei>(attributeBuffers.size());
    glDeleteTextures(numAttributeBuffers, attributeBuffers.data());
    glDeleteRenderbuffers(1, &depthBuffer);

    glDeleteFramebuffers(1, &aliasedFrameBuffer);
    glDeleteTextures(1, &aliasedResult);

    delete xToon;
    delete phong;
    delete ssao;
    delete mlaa;
}


/*----------- Configure ------------------------------------*/

void DeferredRenderer::configureGeometricFrameBuffer(GLsizei width, GLsizei height) {
    const unsigned int num_attributes = 2;

    auto attachments = new unsigned int[num_attributes];
    for (unsigned int i = 0; i < num_attributes; ++i) {
        attachments[i] = GL_COLOR_ATTACHMENT0 + i;
    }

    glGenFramebuffers(1, &geometricFramebuffer);
    glBindFramebuffer(GL_FRAMEBUFFER, geometricFramebuffer);

    attributeBuffers.resize(num_attributes);
    glGenTextures(num_attributes, attributeBuffers.data());

    configureViewDepthMap(width, height, attachments[DEPTH]);
    configureNormalBuffer(width, height, attachments[NORMAL]);

    glBindTexture(GL_TEXTURE_2D, 0);

    glDrawBuffers(num_attributes, attachments);
    delete[] attachments;

    create_depthBuffer(width, height);

    // finally check if frame Buffer is complete
    if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE) {
        throw runtime_error{"Frame Buffer not complete"};
    }

    glBindFramebuffer(GL_FRAMEBUFFER, 0);
}


void DeferredRenderer::configureViewDepthMap(GLsizei width, GLsizei height, unsigned int attachment) const {
    const unsigned int levelOfDetails = 0;
    glBindTexture(GL_TEXTURE_2D, attributeBuffers[DEPTH]);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_R32F, width, height, 0, GL_RED, GL_FLOAT, nullptr);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glFramebufferTexture2D(GL_FRAMEBUFFER, attachment, GL_TEXTURE_2D, attributeBuffers[DEPTH], levelOfDetails);
    // Ensures we don't accidentally over sample position/depth values in screen-space outside the texture's default coordinate region.
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
}


void DeferredRenderer::configureNormalBuffer(GLsizei width, GLsizei height, unsigned int attachment) const {
    const unsigned int levelOfDetails = 0;
    glBindTexture(GL_TEXTURE_2D, attributeBuffers[NORMAL]);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB16F, width, height, 0, GL_RGB, GL_FLOAT, nullptr);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glFramebufferTexture2D(GL_FRAMEBUFFER, attachment, GL_TEXTURE_2D, attributeBuffers[NORMAL], levelOfDetails);
}


// Create and attach depth buffer
void DeferredRenderer::create_depthBuffer(GLsizei width, GLsizei height) {
    glGenRenderbuffers(1, &depthBuffer);
    glBindRenderbuffer(GL_RENDERBUFFER, depthBuffer);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, width, height);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthBuffer);
}


/*---------------------------------------------------------------------------------------*/

void DeferredRenderer::geometry_pass(const Mesh &mesh) const {
    if (mode_wireframe) {
        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    }

    glEnable(GL_DEPTH_TEST);
    glBindFramebuffer(GL_FRAMEBUFFER, geometricFramebuffer);
    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    stylization->activateGeometryShader();
    mesh.draw();
    Shader::deactivate();
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
}


void DeferredRenderer::lightingPass() const {
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    glDisable(GL_DEPTH_TEST);

    ssao->preLightingPass();

    if (mlaa->getIsActivated()) {
        glBindFramebuffer(GL_FRAMEBUFFER, aliasedFrameBuffer);
    } else {
        glBindFramebuffer(GL_FRAMEBUFFER, 0);
    }

    const unsigned long numAttributes = attributeBuffers.size();
    for (unsigned int i = 0; i < numAttributes; ++i) {
        glActiveTexture(GL_TEXTURE0 + i);
        glBindTexture(GL_TEXTURE_2D, attributeBuffers[i]);
    }

    stylization->activateLightingShader();
    stylization->bindNeededTextures_lighting();

    glClearColor(0.11f, 0.071f, 0.137f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT);

    Utility::renderScreenQuad(screenQuad_VAO);
    Shader::deactivate();

    mlaa->passes();
}

//---------------------------------------------------------------------

void DeferredRenderer::update_viewAndModel(const Eigen::Matrix4f &view, const Eigen::Matrix4f &model) {
    stylization->updateViewAndModel(view, model);
}


void DeferredRenderer::update_projection(const Eigen::Matrix4f &projection) {
    stylization->updateProjection(projection);
    ssao->update_projection(projection);
}


void DeferredRenderer::callback_frameBufferSize(unsigned int width, unsigned int height) {
    glBindTexture(GL_TEXTURE_2D, attributeBuffers[DEPTH]);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_R32F, width, height, 0, GL_RED, GL_FLOAT, nullptr);

    glBindTexture(GL_TEXTURE_2D, attributeBuffers[NORMAL]);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB16F, width, height, 0, GL_RGB, GL_FLOAT, nullptr);

    glBindTexture(GL_TEXTURE_2D, aliasedResult);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_FLOAT, nullptr);

    glBindTexture(GL_TEXTURE_2D, 0);

    ssao->callback_frameBufferSize(width, height);
    mlaa->callback_frameBufferSize(width, height);

    glBindRenderbuffer(GL_RENDERBUFFER, depthBuffer);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, width, height);
    glBindRenderbuffer(GL_RENDERBUFFER, 0);
}


void DeferredRenderer::user_interface(const Eigen::Matrix4f &projection) {
    ImGui::Checkbox("wireframe", &mode_wireframe);

    if (ImGui::RadioButton("Phong", stylization == phong)) {
        stylization = phong;
        stylization->updateProjection(projection);
    }

    if (ImGui::RadioButton("XToon", stylization == xToon)) {
        stylization = xToon;
        stylization->updateProjection(projection);
    }

    if (ImGui::CollapsingHeader("Style Options")) {
        stylization->user_interface();
    }

    ssao->user_interface(projection);
    mlaa->user_interface();
}
